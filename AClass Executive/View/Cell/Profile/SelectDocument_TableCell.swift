//
//  SelectDocument_TableCell.swift
//  AClass Executive
//
//  Created by Apple on 05/02/19.
//  Copyright © 2019 Apple1. All rights reserved.
//

import UIKit

class SelectDocument_TableCell: UITableViewCell {

    @IBOutlet weak var lbl_docTitle: RegularLabel!
    @IBOutlet weak var imgVw_radio: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
