//
//  DocumnetsCollectionCell.swift
//  AClass Executive
//
//  Created by Apple1 on 27/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class DocumnetsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var vw_outer: UIView!
    //MARK:- Variables
    var isAddCell: Bool = true {
        didSet {
           // addBtn.isHidden = !isAddCell
            docImage.isHidden = isAddCell
           // btn_change.isHidden = isAddCell
            //removeBtn.isHidden = isAddCell
        }
    }
    
    //MARK:- Clausures
    var addNewDocument:(()->())?
    var removeDocument:(()->())?
    var changeDocument:(()->())?
    
    @IBOutlet weak var btn_change: UIButton!
    //MARK:- IBOutlets
    @IBOutlet weak var lbl_imagename: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var docImage : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Config
    func config(_ document: Document) {
        if document.type == .Pdf {
          docImage.image = UIImage(named: "add")
        }else{
          docImage.setImage(fromUrl: document.url ?? "", defaultImage: UIImage(named: "add"))
        }
        docImage.isHidden = false
        if document.url != nil{
            addBtn.isHidden = true
            btn_change.isHidden = false
        }else{
            addBtn.isHidden = false
            btn_change.isHidden = true
        }
       // lbl_imagename.text = document.name ?? ""
    }
    
    //MARK:- IBAction
    @IBAction func click_change(_ sender: UIButton) {
        addNewDocument?()
    }
    
    @IBAction func addDocument(_ sender: Any) {
        addNewDocument?()
    }
    
    @IBAction func removeDocumentTap() {
        let alertController = UIAlertController(title: nil, message:"Are you sure you want to remove document?", preferredStyle: .alert)
        
        let deleteButton = UIAlertAction(title: "Delete", style: .default, handler: { (action) -> Void in
            self.removeDocument?()
        })
        alertController.addAction(deleteButton)
        
        let  cancelButton = UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in
        })
        alertController.addAction(cancelButton)
        
        (AppDel.window?.rootViewController as? UINavigationController)?.topViewController?.present(alertController, animated: true, completion: nil)
    }
}
