//
//  HomeRidesCell.swift
//  AClass Executive
//
//  Created by Apple1 on 26/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class HomeRidesCell: UITableViewCell {
    
    //Mark:- IBOutlet
    @IBOutlet weak var contactLabel: SemiBoldLabel!
    @IBOutlet weak var callImageView: UIImageView!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var contactBtn: UIButton!
    @IBOutlet weak var completedImage: UIImageView!
    @IBOutlet weak var carNameLabel : UILabel!
    @IBOutlet weak var pickUpLocationLabel : UILabel!
    @IBOutlet weak var dropLocationLabel : UILabel!
    @IBOutlet weak var dateTimeLabel : UILabel!
    @IBOutlet weak var customeNameLabel : UILabel!
    @IBOutlet weak var seatCapacityLabel : UILabel!
    @IBOutlet weak var luggageCapacityLabel : UILabel!
    
    @IBOutlet weak var infantCountLabel: SemiBoldLabel!
    @IBOutlet weak var stack_infants: UIStackView!
    //Mark:- Variables
    var isRideCompleted : Bool?{
        didSet{
            callImageView.isHidden = isRideCompleted ?? false ? true : false
            contactBtn.isHidden = isRideCompleted ?? false ? true : false
            completedImage.isHidden = isRideCompleted ?? false ? false : true
            contactLabel.text = isRideCompleted ?? false ? "Completed" : "Contact"
        }
    }
    var ride : Ride?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //Mark:- Function
    
    func config(_ ride : Ride){
        self.ride = ride
        self.isRideCompleted = ride.isCompleted
        carImageView.setImage(fromUrl: ride.carImage ?? "", defaultImage: UIImage(named: "defaultcar"))
        carNameLabel.text = ride.carModel?.trim() == "" ? "N/A" : ride.carModel
        pickUpLocationLabel.text = ride.sourceLocation?.trim() == "" ? "N/A" : ride.sourceLocation
        dropLocationLabel.text = ride.destinationLocation?.trim() == "" ? "N/A" : ride.destinationLocation
        dateTimeLabel.text = ride.startDate?.trim() == "" ? "N/A" : ride.startDate
        customeNameLabel.text = ride.name?.trim() == "" ? "N/A" : ride.name
        seatCapacityLabel.text = ride.seatCount?.trim() == "" ? "N/A" : ride.seatCount
        luggageCapacityLabel.text = ride.luggageCount?.trim() == "" ? "N/A" : ride.luggageCount
        if let infants =  ride.infants_childs{
            if infants.count > 0{
                infantCountLabel.text = "\(infants.count)"
            }else{
                infantCountLabel.text = "0"
            }
        }
    }
    
    //MARK:- IBAction
    @IBAction func customerCallTap() {
        guard let ride = ride else {
            BaseViewController().showAlert((AppDel.window?.rootViewController as? UINavigationController)?.topViewController, message: AppMessages.noNumber)
            return
        }
        
        if ride.riderNumber == nil {
            BaseViewController().showAlert((AppDel.window?.rootViewController as? UINavigationController)?.topViewController, message: AppMessages.noNumber)
            return
        }
        
        if let url = URL(string: "tel://\(ride.riderNumber ?? "")"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            BaseViewController().showAlert(nil, message: AppMessages.noNumber)
        }
    }
}
