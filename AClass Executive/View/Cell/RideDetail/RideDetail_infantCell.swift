//
//  RideDetail_infantCell.swift
//  AClass Executive
//
//  Created by Apple on 05/02/19.
//  Copyright © 2019 Apple1. All rights reserved.
//

import UIKit

class RideDetail_infantCell: UITableViewCell {

    @IBOutlet weak var imgVw_child: UIImageView!
    @IBOutlet weak var lbl_childAge: RegularLabel!
    @IBOutlet weak var lbl_childTitle: RegularLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
