//
//  Ride.swift
//  AClass Executive
//
//  Created by Apple1 on 08/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation

struct Ride {
    var id : Int?
    var isCompleted : Bool?
    var name : String?
    var startDate : String?
    var sourceLocation : String?
    var sourcePoints : String?
    var destinationLocation : String?
    var riderNumber : String?
    var email : String?
    var seatCount : String?
    var luggageCount : String?
    var carModel : String?
    var carImage : String?
    var rideStatus : String?
    var infants_childs : [String]?
    
    func getRide(_ dic : NSDictionary?)-> Ride{
        var this = Ride()
        guard let dic = dic else {
            return this
        }
        this.id = dic.value(forKey: "trip_id") as? Int
        this.rideStatus = dic.value(forKey: "ride_status") as? String
        this.isCompleted = dic.value(forKey: "is_completed") as? Bool
        this.name = dic.value(forKey: "user_name") as? String
        this.startDate = dic.value(forKey: "start_date_time") as? String
        this.sourceLocation = dic.value(forKey: "source") as? String
        this.sourcePoints = dic.value(forKey: "source_lat_lng") as? String
        this.destinationLocation = dic.value(forKey: "destination") as? String
        this.riderNumber = dic.value(forKey: "rider_mobile_no") as? String
        this.email = dic.value(forKey: "email") as? String
        this.seatCount = dic.value(forKey: "seat_count") as? String
        this.carModel = dic.value(forKey: "car_model") as? String
        this.carImage = dic.value(forKey: "car_thumbnail") as? String
        this.luggageCount = dic.value(forKey: "luggage_count") as? String
        this.infants_childs = dic.value(forKey: "infants_childs") as? [String]
        return this
    }
        
    func getRidesArray(_ data: [NSDictionary]?) -> [Ride] {
        var array = [Ride]()
        guard let data = data else {
            return array
        }
            
        for obj in data {
            array.append(self.getRide(obj))
        }
        return array
    }
        
}

struct Trip {
    var id : Int?
    var status : Int?
    var dateTime : String?
    var tripPickupLocation : String?
    var tripDestinationLocation : String?
    
    func getTrip(_ dic : NSDictionary?)-> Trip{
        var this = Trip()
        guard let dic = dic else {
            return this
        }
        this.id = dic.value(forKey: "trip_id") as? Int
        this.status = dic.value(forKey: "trip_status") as? Int
        this.dateTime = dic.value(forKey: "trip_datetime") as? String
        this.tripPickupLocation = dic.value(forKey: "trip_pickup") as? String
        this.tripDestinationLocation = dic.value(forKey: "trip_destination") as? String
        
        return this
    }
    
}




//"trip": {
//    "trip_id": 324,
//    "trip_status": 0,
//    "trip_datetime": "",
//    "trip_pickup": "",
//    "trip_destination": ""
//},
//user: {
//    "user_id":" ",
//    "user_name": ""
//    "user_phone": "",
//    "user_email": "",
//},
//car: {
//    "car_id": "",
//    "car_seat": "",
//    "car_luggage": "",
//    "car_model": "",
//    "car_thumbnail": ""
//}

