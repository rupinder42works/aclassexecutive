//
//  User.swift
//  Magic
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class User: NSObject, NSCoding {
    var email: String?
    var id: Int?
    var bio : String?
    var profileImage : String?
    var name: String?
    var phone : String?
    var rides : Int?
    
    static let shared = User()
    
    override init() {
        
    }
    
    init(id: Int?, email: String?, profileImage: String?, bio: String?,name : String?, phone: String?,rides : Int?) {
        self.id = id
        self.profileImage = profileImage
        self.name = name
        self.phone = phone
        self.bio = bio
        self.email = email
        self.rides = rides
    }
  
    
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as? Int
        let email = aDecoder.decodeObject(forKey: "email") as? String
        let profileImage = aDecoder.decodeObject(forKey: "path") as? String
        let name = aDecoder.decodeObject(forKey: "name") as? String
        let rides = aDecoder.decodeObject(forKey: "rides") as? Int
        let phone = aDecoder.decodeObject(forKey: "phone") as? String
        let bio = aDecoder.decodeObject(forKey: "bio") as? String
        self.init(id: id, email: email, profileImage: profileImage, bio: bio,name : name, phone: phone,rides : rides)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(profileImage, forKey: "path")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(bio, forKey: "bio")
        aCoder.encode(rides, forKey: "rides")
    }
    
    func setData(_ dic: NSDictionary) {
        if let data = dic.value(forKey: "user_profile") as? NSDictionary {
            self.id =  Int(data.value(forKey: "user_id") as? String ?? "") ?? 0
            self.email = data.value(forKey: "email") as? String
            self.profileImage = data.value(forKey: "picture") as? String
            self.name = data.value(forKey: "name") as? String
            self.phone = data.value(forKey: "phone") as? String
            self.bio = data.value(forKey: "description") as? String
            self.rides = data.value(forKey: "rides") as? Int
        }        
    }
   
}
