//
//  ProfileData.swift
//  AClass Executive
//
//  Created by Apple1 on 08/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation

struct ProfileData{
    var userDetail : User?
    var userDocuments : [Document]?
    
    init() {
        
    }
    
    func getProfileData(_ dic : NSDictionary?) -> ProfileData {
        var this = ProfileData()
        guard let dic = dic else {
            return this
        }
        this.userDocuments = Document().getDocumentsArray( dic.value(forKey: "documents") as? [NSDictionary])
        
        return this
    }
}


enum DocumentType : String{
    case Pdf = "Pdf"
    case Image = "Image"
}


struct Document{
    var id : Int?
    var url : String?
    var status : Bool?
    var name: String?
    var type : DocumentType?
    var docType : String?
    
    func getDocuments(_ dic : NSDictionary?)-> Document{
        var this = Document()
        guard let dic = dic else {
            return this
        }
        this.id = dic.value(forKey: "doc_id") as? Int
        this.url = dic.value(forKey: "doc_url") as? String
        this.type = ((dic.value(forKey: "doc_type") as? String)?.contains("pdf") ?? false) ? .Pdf : .Image
        this.name = dic.value(forKey: "doc_name") as? String
        this.status = dic.value(forKey: "doc_status") as? Bool
        
        return this
    }
    
    func getDocumentsArray(_ data: [NSDictionary]?) -> [Document] {
        var array = [Document]()
        guard let data = data else {
            return array
        }
        
        for obj in data {
            array.append(self.getDocuments(obj))
        }
        return array
    }
    
}
