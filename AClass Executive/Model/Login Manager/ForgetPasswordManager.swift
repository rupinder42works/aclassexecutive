//
//  ForgetPasswordManager.swift
//  Magic
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class ForgotManager {
    var email: String?
    
    
    //MARK:- Form Validations
    private func isValidData() -> (Bool, String) {
        var message = ""
        if email == "" {
            message = ValidationMessage.emptyEmail
        } else if !(email?.isValidEmail() ?? true){
            message = ValidationMessage.invalidEmail
        }
        
        return message == "" ? (true, message) : (false, message)
    }
    
    //MARK:- Validation
    func isFormValid() -> (Bool, String) {
        return (isValidData().0, isValidData().1)
    }
}
