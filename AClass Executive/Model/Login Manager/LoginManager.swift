//
//  LoginManager.swift
//  Magic
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class LoginModel {
    var email: String?
    var pwd: String?
    
    convenience init(email: String, pwd: String) {
        self.init()
        self.email = email
        self.pwd = pwd        
    }

    
    //MARK:- Form Validations
    private func isValidData() -> (Bool, String) {
        var message = ""
        if email == "" {
            message = ValidationMessage.emptyEmail
        }else if !(email?.isValidEmail() ?? false){
            message = ValidationMessage.invalidEmail
        }else if pwd == "" {
            message = ValidationMessage.emptyPwd
        }
        
        return message == "" ? (true, message) : (false, message)
    }
    
    //MARK:- Validation
    func isFormValid() -> (Bool, String) {
        return (isValidData().0, isValidData().1)
    }
}

