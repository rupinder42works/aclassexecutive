//
//  RideViewModel.swift
//  AClass Executive
//
//  Created by Apple1 on 09/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation

enum RideStatus {
    case upcoming
    case past
    case all

    func getRideStatus() -> String {
        var status = ""
        switch self {
        case .past:
            status = "past"
            break
        case .upcoming:
            status = "upcoming"
            break
        default:
            status = "null"
            break
        }
        return status
    }
}

class QueueOrder {
    
    var completedRides: [Ride]?

    
    private static var sharedQueueOrder: QueueOrder = {
        return QueueOrder()
    }()
    
    //MARK:- Accessor
    class func shared() -> QueueOrder {
        return sharedQueueOrder
    }
}


class RideViewModel: BaseViewModel {
    //Mark:- Clausures
    var refreshOrdersClausre:(()->())?
    
    //MARK:- Variables
    var rideStatus: RideStatus = .all {
        didSet {
          getRidesList(rideStatus,false)
        }
    }
    var page : Int = 0
    var rideArr: [Ride]?
    var limit = 50
    var isLoadMoreRequired: Bool = true
    var ride : Ride?
    //var currentRide : Ride?
    var status : String?{
        didSet{
            updateRideStatus()
        }
    }
    var isRefreshRequiredTabs: Bool! {
        didSet {
            refreshOrdersClausre?()
        }
    }
    

    
    override init() {
        super.init()
        rideArr = [Ride]()
    }
    
    
     //MARK:- Service Call
    func getRidesList(_ rideStatus: RideStatus,_ isRefresh: Bool = false){
        if !isRefresh {
            isLoading = true
        }
        
        page = ((rideArr?.count ?? 0)/limit) + 1
        if isRefresh {
            page = 1
            self.showEmptyMsg = false
            self.isEndRefresh = true
        }
        let rideType = rideStatus == .upcoming ? "upcoming" : "completed"
        
        let dic = ["user_email" : Utility.sharedInstance.currentUserEmail ?? "", "device_id" : UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? "","ride_type" : rideType,"page_no" : page,"page_count" : limit] as [String : Any]
        
        WebServices().postMainRequest(methodName: ServiceName.homeListng, params: dic as NSDictionary , oncompletion: { (status, message, response) in
            if isRefresh {
                self.isEndRefresh = true
            } else {
                self.isLoading = false
            }
            
            if status ?? false {
                if isRefresh{
                    self.rideArr?.removeAll()
                }
                if let dataArr = (response?.value(forKey: "data") as? NSDictionary)?.value(forKey: "ride_list") as? [NSDictionary]{
                    for dic in dataArr{
                        let ride = Ride().getRide(dic)
                        self.rideArr?.append(ride)
                    }
                }
                
                if self.rideArr?.first?.isCompleted == false{
                    if let currentRideDic = (response?.value(forKey: "data") as? NSDictionary)?.value(forKey: "current_ride") as? NSDictionary{
                        Utility.sharedInstance.currentRide = Ride().getRide(currentRideDic)
                    }else{
                        Utility.sharedInstance.currentRide = nil
                    }
                    
                }
                
                self.responseReceived?()
                if (self.rideArr?.count ?? 0)/self.page % self.limit == 0 {
                    self.isLoadMoreRequired = true
                } else {
                    self.isLoadMoreRequired = false
                }
               
            } else {
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }
                }else{
                    self.alertMessage = message
                }
            }
            
            if self.rideArr?.count == 0 {
                self.showEmptyMsg = true
            }
        })
        
    }
  

    private func updateRideStatus(){
        isLoading = true
        let dic = ["user_email" : Utility.sharedInstance.currentUserEmail ?? "", "device_id" : UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? "","ride_id" : ride?.id ?? 0,"status" : status ?? ""] as [String : Any]
         WebServices().postMainRequest(methodName: ServiceName.updateRideStatus, params: dic as NSDictionary , oncompletion: { (status, message, response) in
            self.isLoading = false   
            if status ?? false{
                self.alertMessage = message
               // self.handleOrder()
                self.responseReceived?()
            }else{
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }
                }else{
                    self.alertMessage = message
                }
            }
            
        })
        
    }
    
//    func rideCompleted() {
//        if let arr = QueueOrder.shared().completedRides, arr.count > 0 {
//            for ride in arr {
//                var ride = ride
//                ride.isCompleted = true
//                ride.rideStatus = "3"
//                rideArr?.insert(ride, at: 0)
//                reloadTableViewClosure?()
//            }
//
//            QueueOrder.shared().completedRides?.removeAll()
//        }
//    }
//
//    func handleOrder(){
//        if status == "3"{
//            if QueueOrder.shared().completedRides == nil {
//
//                QueueOrder.shared().completedRides = [Ride]()
//            }
//            ride?.rideStatus = "3"
//            ride?.isCompleted = true
//            QueueOrder.shared().completedRides?.append(ride ?? Ride())
//            self.isRefreshRequiredTabs = true
//        }
//    }
}
