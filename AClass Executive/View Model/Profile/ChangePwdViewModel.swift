//
//  ChangePwdViewModel.swift
//  AClass Executive
//
//  Created by Apple1 on 08/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation

class ChangePwdViewModel: BaseViewModel {
    
    //MARK:- Variables
    
    var oldPwd: String
    var newPwd: String
    var confirmPwd: String
    
    //MARK:- Clausures
    var passwordChanged:(()->())?
    
    //MARK:- Initialise
    init(oldPwd: String, newPwd: String, confirmPwd: String) {
        self.oldPwd = oldPwd
        self.newPwd = newPwd
        self.confirmPwd = confirmPwd
    }
    
    //MARK:- Functions
    func pwdChange() {
        let response = isFormValid()
        if response.0 {
            self.changePwdService()
        } else {
            self.alertMessage = response.1
        }
    }
    
    //MARK:- Private methods
    private func isFormValid() -> (Bool, String) {
        var errorMsg = ""
        if oldPwd == "" {
            errorMsg = ValidationMessage.emptyOldPwd
        } else if newPwd == "" {
            errorMsg = ValidationMessage.emptyNewPwd
        } else if confirmPwd == "" {
            errorMsg = ValidationMessage.emptyConfirmPwd
        } else if newPwd != confirmPwd {
            errorMsg = ValidationMessage.confirmPwdNoMatch
        }
        return errorMsg == "" ? (true, "") : (false, errorMsg)
    }
    
    private func changePwdService() {
        self.isLoading = true
        let dic = ["old_password": oldPwd, "new_password": newPwd, "user_email": Utility.sharedInstance.getUserData().email ?? "","device_id" : UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? ""] as [String : Any]
     
        WebServices().postMainRequest(methodName: ServiceName.changePwd, params: dic as NSDictionary , oncompletion: { (status, message, response) in
            
            self.isLoading = false
            if status ?? false {
                if let response = response {
                    self.alertMessage = response.value(forKey: "message") as? String ?? ""
                    self.passwordChanged?()
                }
            } else {
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }
                }else{
                    self.alertMessage = message
                }
            }
        })
    }
}
