//
//  ProfileViewModel.swift
//  AClass Executive
//
//  Created by Apple1 on 08/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewModel: BaseViewModel {
    //MARK:- Variables
    var isLogout : Bool?{
        didSet{
            logoutUser()
        }
    }
    
    var retrieveUserProfile : Bool?{
        didSet{
            getUserProfile()
        }
    }
    
    var logoutResponseSuccess: Bool? {
        didSet {
            self.logoutSuccess?()
        }
    }
    var profileResponseSuccess: Bool? {
        didSet {
            // self.logoutSuccess?()
        }
    }
    var logoutSuccess:(()->())?
    var userProfileRetrieved:(()->())?
    var user : User?
    var userDocuments : [Document]?
    var profileImage : UIImage?
    
    //MARK:- Service Call
    private func logoutUser(){
        isLoading = true
        
        let dic = ["user_id": Utility.sharedInstance.currentUserId, "device_id": UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? ""] as [String : Any]
        
        WebServices().postMainRequest(methodName: ServiceName.logout, params: dic as NSDictionary , oncompletion: { (status, message, response) in
            
            self.isLoading = false
            if status ?? false {
                self.logoutSuccess?()
            } else {
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }
                }else{
                    self.alertMessage = message
                }
            }
        })
        
    }
    
    private func getUserProfile(){
        isLoading = true
        let dic = ["user_email" : Utility.sharedInstance.currentUserEmail,"device_id": UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? ""]
        WebServices().postMainRequest(methodName: ServiceName.viewProfile, params: dic as NSDictionary , oncompletion: { (status, message, response) in
            self.isLoading = false
            if status ?? false {
                self.user = User()
                self.user?.setData(response?.value(forKey: "data") as? NSDictionary ?? NSDictionary())
                Utility.sharedInstance.currentUserId = self.user?.id ?? 0
                Utility.sharedInstance.currentUserEmail = self.user?.email
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: self.user ?? User())
                UserDefaults.standard.set(encodedData, forKey: UserDefault.user)
                UserDefaults.standard.synchronize()
                
                self.userDocuments = Document().getDocumentsArray((response?.value(forKey: "data") as? NSDictionary)?.value(forKey: "documents") as? [NSDictionary])
                self.userProfileRetrieved?()
            } else {
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }else{
                        self.alertMessage = message
                    }
                }
            }
        })
    }
    
    func uploadUserProfilePicture() {
        let imgData = profileImage?.jpegData(compressionQuality: 0.5) ?? Data()
        let imgDic = ["file": imgData,"user_email" : Utility.sharedInstance.currentUserEmail ?? "","device_id" : UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? ""] as NSDictionary
        isLoading = true
        WebServices().uploadImage(methodName: ServiceName.updateProfilePicture, params: imgDic as NSDictionary) { (status, message, response) in
            self.isLoading = false
            if status ?? false {
                if let response = response {
                    self.user = User()
                    self.user?.setData(response)
                    Utility.sharedInstance.currentUserId = self.user?.id ?? 0
                    Utility.sharedInstance.currentUserEmail = self.user?.email
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: self.user ?? User())
                    UserDefaults.standard.set(encodedData, forKey: UserDefault.user)
                    UserDefaults.standard.synchronize()
                }
            } else {
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }
                }else{
                    self.alertMessage = message
                }
            }
        }
    }
    
    func removeDocument(_ index: Int, complete: @escaping (Bool)-> Void) {
        guard let document = self.userDocuments?[index] else {
            return
        }
        
        let dic = ["user_email": Utility.sharedInstance.currentUserEmail ?? "",
                   "device_id": UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? "",
                   "doc_name": document.name]
        isLoading = true
        WebServices().postMainRequest(methodName: ServiceName.removeDocument, params: dic as NSDictionary) { (status, message, response) in
            self.isLoading = false
            if status ?? false {
                if let response = response {
                    print(response)
                    if index < self.userDocuments?.count ?? 0{
                        self.userDocuments?.remove(at: index)
                    }
                    complete(true)
                    return
                }
                complete(false)
            } else {
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }
                }else{
                    self.alertMessage = message
                }
                
            }
        }
    }
}
