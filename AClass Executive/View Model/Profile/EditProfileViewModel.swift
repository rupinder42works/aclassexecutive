//
//  EditProfileViewModel.swift
//  AClass Executive
//
//  Created by Apple1 on 08/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation


class EditProfileViewModel: BaseViewModel {
    
    //MARK:– Variables
    var name: String?
    var email: String?
    var phone: String?
    var bio : String?
    var responseSuccess: NSDictionary? {
        didSet {
            self.editProfileSuccess?()
        }
    }
    
    //MARK:- Clousures
    var editProfileSuccess:(()->())?
    var sessionExpired:(()->())?
    
    //MARK:- Initialisation
    convenience init(name: String?, email: String?, phone: String?,bio: String?) {
        self.init()
        self.name = name
        self.email = email
        self.phone = phone
        self.bio = bio
    }
    
    
    //MARK:- Function
    func editProfile() {
        let response = isFormValid()
        if response.0 {
            editProfileService()
        } else {
            self.alertMessage = response.1
        }
    }
    
    //MARK:- Private methods
    private func isFormValid() -> (Bool, String) {
        var errorMsg = ""
        if name == "" {
            errorMsg = ValidationMessage.emptyName
        } else if email == "" {
            errorMsg = ValidationMessage.emptyEmail
        } else if phone == "" {
            errorMsg = ValidationMessage.emptyPhone
        } else if bio == "" {
            errorMsg = ValidationMessage.emptyBio
        }else if !(email?.isValidEmail() ?? false) {
            errorMsg = ValidationMessage.invalidEmail
        }else if phone?.isValidPhone() ?? false{
            errorMsg = ValidationMessage.invalidPhone
        }else if (phone?.length ?? 0) < 5 || (phone?.length ?? 0) > 15{
            errorMsg = ValidationMessage.invalidPhone
        }
        return errorMsg == "" ? (true, "") : (false, errorMsg)
    }
    
     //MARK:- Service Call
    private func editProfileService() {
        isLoading = true
        let dic = ["device_id" : UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? "","user_email": Utility.sharedInstance.currentUserEmail ?? "", "name": name ?? "","email" : email ?? "","phone" : phone ?? "" , "description" : bio ?? ""] as [String : Any]
        
        WebServices().postMainRequest(methodName: ServiceName.editProfile, params: dic as NSDictionary , oncompletion: { (status, message, response) in
            
            self.isLoading = false
            if status ?? false {
                if let response = response {
                    let user = User()
                    user.setData(response)
                    Utility.sharedInstance.currentUserId = user.id ?? 0
                    Utility.sharedInstance.currentUserEmail = user.email
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: user)
                    UserDefaults.standard.set(encodedData, forKey: UserDefault.user)
                    UserDefaults.standard.synchronize()
                    self.responseSuccess = response
                    self.alertMessage = message
                }
            } else {
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                       Utility.sharedInstance.logoutUser()
                    }
                }else{
                     self.alertMessage = message
                }
            }
        })
    }
}
