//
//  DocumentViewModel.swift
//  AClass Executive
//
//  Created by Apple1 on 11/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation
import UIKit

class DocumentViewModel : BaseViewModel{
    
    //MARK:- Variables
    var newDocumentArr:(([Document])->())?
    var docType : DocumentType?
    var docName : String?
    var selectedPdfUrl : URL?{
        didSet{
            uploadDoc()
        }
    }
    var selectedImage : UIImage?{
        didSet{
            uploadDoc()
        }
    }
    
    //MARK:- Clausure
    
    private func uploadDoc(){
        var imgData = Data()
        if docType == .Image{
            imgData = selectedImage?.jpegData(compressionQuality: 0.5) ?? Data()
        }else{
            imgData = try! Data(contentsOf: selectedPdfUrl ?? URL(fileURLWithPath: ""))
        }
        
        let imgDic = ["file": imgData,"user_email" : Utility.sharedInstance.currentUserEmail ?? "","device_id" : UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? "","type" : docType?.rawValue ?? "","doc_name" : docName ?? ""] as NSDictionary
        isLoading = true
        WebServices().uploadDocument(methodName: ServiceName.documentUpload, params: imgDic as NSDictionary) { (status, message, response) in
            self.isLoading = false
            if status ?? false {
                if let response = response {
                    self.responseReceived?()
                    self.newDocumentArr?(Document().getDocumentsArray((response.value(forKey: "data") as? NSDictionary)?.value(forKey: "documents") as? [NSDictionary]))
                    self.alertMessage = message
                }
            } else{
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }
                }else{
                    self.alertMessage = message
                }
            }
        }
    }
    
}
