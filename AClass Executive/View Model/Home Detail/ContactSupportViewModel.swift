//
//  ContactSupportViewModel.swift
//  AClass Executive
//
//  Created by Apple1 on 10/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation

class ContactSupportViewModel: BaseViewModel {
    
    //MARK:– Variables
    var firstName: String?
    var lastName: String?
    var email: String?
    var subject: String?
    var message : String?
    var responseSuccess: NSDictionary? {
        didSet {
            self.contactSupportSuccess?()
        }
    }
    
    //MARK:- Clousures
    var contactSupportSuccess:(()->())?
    
    
    //MARK:- Initialisation
   convenience init(firstName: String?,lastName : String?, email: String?, subject: String?,message: String?){
        self.init()
        self.firstName = firstName
        self.email = email
        self.lastName = lastName
        self.subject = subject
        self.message = message
    }
    
    
    //MARK:- Function
    func contactSupport() {
        let response = isFormValid()
        if response.0 {
            contactSupportService()
        } else {
            self.alertMessage = response.1
        }
    }
    
    //MARK:- Private methods
    private func isFormValid() -> (Bool, String) {
        var errorMsg = ""
        if firstName == "" {
            errorMsg = ValidationMessage.firstName
        } else if lastName == "" {
            errorMsg = ValidationMessage.lastName
        }else if email == "" {
            errorMsg = ValidationMessage.emptyEmail
        } else if subject == "" {
            errorMsg = ValidationMessage.subject
        } else if message == "" {
            errorMsg = ValidationMessage.message
        }else if !(email?.isValidEmail() ?? false) {
            errorMsg = ValidationMessage.invalidEmail
        }
        return errorMsg == "" ? (true, "") : (false, errorMsg)
    }
    
    //MARK:- Service Call
    private func contactSupportService() {
        isLoading = true
        let dic = ["device_id" : UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? "","user_email": Utility.sharedInstance.currentUserEmail ?? "", "first_name": firstName ?? "","last_name" : lastName ?? "", "email_id" : email ?? "","subject" : subject ?? "" , "message" : message ?? ""] as [String : Any]
        
        WebServices().postMainRequest(methodName: ServiceName.contactSupport, params: dic as NSDictionary , oncompletion: { (status, message, response) in
            
            self.isLoading = false
            if status ?? false {
                if let response = response {
                    self.responseSuccess = response
                    self.alertMessage = message
                }
            } else {
                if let isLoggedOut = response?.value(forKey: "is_loggedout")as? Bool{
                    if isLoggedOut{
                        Utility.sharedInstance.logoutUser()
                    }
                }else{
                    self.alertMessage = message
                }
            }
        })
    }
}
