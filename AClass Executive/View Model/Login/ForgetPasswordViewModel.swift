//
//  ForgetPasswordViewModel.swift
//  AClass Executive
//
//  Created by Apple1 on 08/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation

class ForgetPasswordViewModel : BaseViewModel{
    
    var model : ForgotManager? 
    
    var responseSuccess: NSDictionary? {
        didSet {
            self.emailSentSuccess?()
        }
    }
    
    func validateData(_ model: ForgotManager) {
        self.model = model
        self.forgotPwdService()
    }
    
    //MARK:- Clausers
    var emailSentSuccess:(()->())?
    
    private func serverData() -> NSDictionary {
        var serverDic = [String: String]()
        serverDic["email"] = self.model?.email ?? ""
        return serverDic as NSDictionary
    }
    
    //MARK:- Service Calling
    
    func forgotPwdService() {
        isLoading = true
        let dic = serverData()
        WebServices().postRequest(methodName: ServiceName.forgotPwd, params: dic , oncompletion: { (status, message, response) in
            self.isLoading = false
            if status ?? false {
                self.responseSuccess = response
            } else {
                self.alertMessage = message
            }
        })
    }
}
