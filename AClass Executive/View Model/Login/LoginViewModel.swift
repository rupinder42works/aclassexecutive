//
//  LoginViewModel.swift
//  Magic
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class LoginViewModel: BaseViewModel {
    
    //MARK:- Variables
    var model: LoginModel? = nil
    
    var responseSuccess: NSDictionary? {
        didSet {
            self.loginSuccess?()
        }
    }
    
    func validateData(_ model: LoginModel) {
        self.model = model
        self.loginService()
    }
    
    
    //MARK:- Clausers
    var loginSuccess:(()->())?
    
    private func serverData() -> NSDictionary {
        var serverDic = [String: String]()
        serverDic["username"] = self.model?.email ?? ""
        serverDic["password"] = self.model?.pwd ?? ""
        serverDic["device_id"] = UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? ""
        serverDic["device_token"] = AppDel.fcmToken
        serverDic["device_type"] = "iOS"
        return serverDic as NSDictionary
    }
    
    //MARK:- Service Calling
    func loginService() {
        isLoading = true
        let dic = self.serverData()
        WebServices().postMainRequest(methodName: ServiceName.login, params: dic , oncompletion: { (status, message, response) in
            
            self.isLoading = false
            if status ?? false {
                if let response = response {
                    if let data = response.value(forKey: "data") as? NSDictionary{
                        let user = User()
                        user.setData(data)
                        Utility.sharedInstance.currentUserId = user.id ?? 0
                        Utility.sharedInstance.currentUserEmail = user.email
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: user)
                        UserDefaults.standard.set(encodedData, forKey: UserDefault.user)
                        UserDefaults.standard.synchronize()
                        self.responseSuccess = response
                    }
                }
            } else {
                self.alertMessage = message
            }
        })
    }
}
