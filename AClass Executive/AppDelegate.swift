//
//  AppDelegate.swift
//  AClass Executive
//
//  Created by Apple1 on 24/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import UserNotifications

let AppDel = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var fcmToken = ""
    var isIPad: Bool = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusbar.backgroundColor = AppColor.darkTextColor
        }
        


        UIApplication.shared.statusBarStyle = .lightContent
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        //Config Firebase
        FirebaseApp.configure()
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        
        self.registerPushNotification()
        UNUserNotificationCenter.current().delegate = self
        
        // [END register_for_notifications]
        
        //set device Id
        UserDefaults.standard.set(UIDevice.current.identifierForVendor!.uuidString, forKey: UserDefault.deviceId)
        
        //Check if user is logged in
        self.checkFlow()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    private func checkFlow() {
        if let userData = UserDefaults.standard.object(forKey: UserDefault.user) as? Data {
            var user = User.shared
            user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? User ?? User()
            print(user)
            Utility.sharedInstance.currentUserId = user.id ?? 0
            Utility.sharedInstance.currentUserEmail = user.email
            
            
            let loginVC = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateInitialViewController()
            
            let navigationController = UINavigationController()
            navigationController.setViewControllers([loginVC!], animated: true)
            navigationController.isNavigationBarHidden = true
            self.window?.rootViewController = navigationController
        } else {
            let loginVC = UIStoryboard(name: StoryboardName.login, bundle: nil).instantiateInitialViewController()
            self.window?.rootViewController = loginVC
        }
    }

    func logoutTap() {
        let loginVC = UIStoryboard(name: StoryboardName.login, bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController = UINavigationController()
        navigationController.setViewControllers([loginVC], animated: true)
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        
        loginVC.view.makeToast(AppMessages.logout, duration: 3.0, point: loginVC.view.center, title: nil, image: nil) { didTap in
        }
    }
    
    func sessionExpired() {
        let loginVC = UIStoryboard(name: StoryboardName.login, bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController = UINavigationController()
        navigationController.setViewControllers([loginVC], animated: true)
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        
        loginVC.view.makeToast(AppMessages.sessionExpired, duration: 3.0, point: loginVC.view.center, title: nil, image: nil) { didTap in
        }
    }
    
    
    
    fileprivate func setupNotifications() {
        let nc = NotificationCenter.default
        nc.addObserver(forName: NSLocale.currentLocaleDidChangeNotification, object: nil, queue: OperationQueue.main) {
            [weak self] notification in
            guard let `self` = self else { return }
            
            let sb = UIStoryboard(name: "Home", bundle: nil)
            let vc = sb.instantiateInitialViewController()
            self.window?.rootViewController = vc
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        print(userInfo)
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func registerPushNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]) { (status, error) in
            print("permission granted: \(status)")
            
            guard status else {
                return
            }
            
            if #available(iOS 11.0, *) {
                let viewAction = UNNotificationAction(identifier: "view", title: "View Title", options: [.foreground])
                
                let categoryAction = UNNotificationCategory(identifier: "NEWS_CATEGORY", actions: [viewAction], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "xyz", options: [])
                UNUserNotificationCenter.current().setNotificationCategories([categoryAction])
            } else {
                // Fallback on earlier versions
            }
            self.getNotificationSetting()
        }
    }
    
    func getNotificationSetting() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("notification settings: \(settings)")
            
            guard settings.authorizationStatus == .authorized else {
                return
            }
            
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
            })
        }
    }
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
      
        // Print full message.
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.received), object: userInfo)
        // Change this to your preferred presentation option
        completionHandler([.alert, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
      
        // Print full message.
        print(userInfo)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.received), object: nil)
        completionHandler()
        
        if let navController = self.window?.rootViewController as? UINavigationController {
            let detailVC = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            navController.viewControllers.append(detailVC)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        
        print("token:\(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let aps = userInfo["aps"] as! [String: AnyObject]

        completionHandler(.newData)
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        self.fcmToken = fcmToken
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}
