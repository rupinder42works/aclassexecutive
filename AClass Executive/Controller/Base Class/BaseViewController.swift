//
//  BaseViewController.swift
//  GoDolly
//
//  Created by 42Works-Worksys2 on 15/05/18.
//  Copyright © 2018 42works. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    
    
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
    //MARK:- Get User
    func getUser() -> User {
        if let userData = UserDefaults.standard.object(forKey: UserDefault.user) as? Data {
            let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? User ?? User()
            return user
        }
        return User()
    }
    
    //MARK:- Show Alert
    func showAlert(_ viewController: UIViewController?, message: String) {
        if let vc = viewController {
            vc.view.makeToast(message, duration: 5.0, position: .center)
        } else {
            AppDel.window?.topMostController()?.view.makeToast(message, duration: 5.0, position: .center)
        }
    }
    
    func showLoader(_ viewController: UIViewController?) {
        if let vc = viewController {
            vc.view.makeToastActivity(.center)
        }
    }
    
    func hideLoader(_ viewController: UIViewController?) {
        if let vc = viewController {
            vc.view.hideToastActivity()
        }        
    }
    
    func fullScreenLoaderWithTap(_ viewController: UIViewController, message: String, onCompletion:@escaping (Bool?) -> Void) {
        viewController.view.makeToast(message, duration: 5.0, point: viewController.view.center, title: nil, image: nil) { didTap in
            onCompletion(didTap)
        }
    }
    

    //MARK:- EmptyMessage
    func showEmptyMsg(_ message: String, superView: UIView?, align: NSTextAlignment = .center, leadingSubtract: Int = 0) {
        self.removeEmptyMsg(superView: superView)
        let label = UILabel()
        label.tag = -10
        label.text = message
        label.font = UIFont(name: "Roboto-Regular", size: 16.0)
        label.frame = CGRect(x: CGFloat(10 - leadingSubtract), y: 0.0, width: superView?.frame.width ?? 100 - 20, height: superView?.frame.height ?? 20.0)
        label.textAlignment = align
        label.textColor = AppColor.darkGrayColor
        label.numberOfLines = 0
        superView?.addSubview(label)
        
    }
    
    func showEmptyScreen(_ message: String, superView: UIView?, image: UIImage? = UIImage(named: "Noride")) {
        self.removeEmptyMsg(superView: superView)
        let emptyImage = UIImageView()
        emptyImage.tag = -10
        emptyImage.image = image
        emptyImage.contentMode = .scaleAspectFit
        emptyImage.frame = CGRect(x: (superView?.frame.size.width ?? 100)/2 - 100, y: (superView?.frame.size.height ?? 100)/2 - 150, width: 200, height: 200)
        superView?.addSubview(emptyImage)
        
        let label = UILabel()
        label.tag = -10
        label.text = message
        label.font = UIFont(name: "Roboto-Regular", size: 16.0)
        label.frame = CGRect(x: 5.0, y: 20.0, width: (superView?.frame.size.width ?? 100) - 20, height: (superView?.frame.size.height ?? 100))
        label.textAlignment = .center
        label.textColor = AppColor.darkTextColor
        label.numberOfLines = 0
        superView?.addSubview(label)
    }
    
    func removeEmptyMsg(superView: UIView?) {
        guard let superView = superView else {
            return
        }
        for view in superView.subviews {
            if let temp = view as? UILabel, temp.tag == -10 {
                view.removeFromSuperview()
            }
            if let temp = view as? UIImageView, temp.tag == -10 {
                view.removeFromSuperview()
            }
            if let temp = view as? UILabel, temp.tag == -20 {
                view.removeFromSuperview()
            }
        }
    }
    
    func showTwoLineEmptyScreen(_ message: String, message2 : String, superView: UIView?, image: UIImage = #imageLiteral(resourceName: "cartImage")) {
        self.removeEmptyMsg(superView: superView)
        let emptyImage = UIImageView()
        emptyImage.tag = -10
        emptyImage.image = image
        emptyImage.contentMode = .center
        emptyImage.frame = CGRect(x: (((superView?.frame.size.width) ?? 100) + 10)/2 - 100, y: ((superView?.frame.size.height) ?? 100 - 10)/2 - 150, width: 200, height: 200)
        superView?.addSubview(emptyImage)
        
        let label = UILabel()
        label.tag = -10
        label.text = message
        label.font = UIFont(name: "Roboto-Regular", size: 16.0)
        label.center.x = emptyImage.center.x
        label.frame = CGRect(x: 5.0, y: 0.0 + 30, width: (superView?.frame.width) ?? 100 - 20, height: (superView?.frame.height) ?? 100)
        label.textAlignment = .center
        label.textColor = AppColor.darkTextColor
        label.numberOfLines = 0
        superView?.addSubview(label)
        
        let label2 = UILabel()
        label2.tag = -20
        label2.text = message2
        label2.font = UIFont(name: "Roboto-Light", size: 14.0)
        label2.frame = CGRect(x: 5.0, y: label.frame.origin.y + 20 , width: (superView?.frame.width) ?? 100 - 20, height: (superView?.frame.height) ?? 100)
        label2.textAlignment = .center
        label2.textColor = AppColor.darkTextColor
        label2.numberOfLines = 0
        superView?.addSubview(label2)
    }

    
    //MARK:- IBAction
    @IBAction func backTap() {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension UILabel {
    func removeLabel() {
        self.removeFromSuperview()
    }
}

