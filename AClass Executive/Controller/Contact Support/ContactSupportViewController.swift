//
//  ContactSupportViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 28/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.-
//

import UIKit

class ContactSupportViewController: BaseViewController {
    
    //MARK:- Variables
    var contactSupportVM : ContactSupportViewModel?
    
    //MARK:- IBOutlets
    @IBOutlet weak var firstNameTextField : UITextField!
    @IBOutlet weak var lastNameTextField : UITextField!
    @IBOutlet weak var subjectTextField : UITextField!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var messageTextView : UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func contactBtnTap(){
        contactSupportVM = ContactSupportViewModel()
        
        contactSupportVM?.email = emailTextField.text?.trim() ?? ""
        contactSupportVM?.firstName = firstNameTextField.text?.trim() ?? ""
        contactSupportVM?.lastName = lastNameTextField.text?.trim() ?? ""
        contactSupportVM?.subject = subjectTextField.text?.trim() ?? ""
        contactSupportVM?.message = messageTextView.text?.trim() ?? ""
        
        
        contactSupportVM?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.contactSupportVM?.isLoading ?? false {
                    self?.showLoader(self)
                } else {
                    self?.hideLoader(self)
                }
            }
        }
        
        contactSupportVM?.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.contactSupportVM?.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        contactSupportVM?.contactSupportSuccess = { [weak self] () in
           
        }
        
        contactSupportVM?.contactSupport()
        
        
        
        
    }

}
