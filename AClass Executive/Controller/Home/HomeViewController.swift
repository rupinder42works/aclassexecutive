//
//  HomeViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 26/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController{

    //MARK:- Variables
    
    var selectedTap = 0
    var newOrdersCount = 0
    lazy var refProductController = [HomeRidesViewController]()
    
    //MARK:- IBOutlets
    @IBOutlet var topButtons: [UIButton]!
    @IBOutlet var mainScrollView: UIScrollView! {
        didSet {
            mainScrollView.delegate = self
        }
    }
    @IBOutlet weak var scrollLineLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImage: UIImageView!

    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if getUser().profileImage != nil{
            profileImage.setImage(fromUrl: getUser().profileImage ?? "", defaultImage: UIImage(named: "Default_User"))
        }
    }
    
    //MARK:- IBActions
    @IBAction func topTaps(_ sender: UIButton) {
        selectedTap = sender.tag - 1
        if selectedTap == 1
        {
            if Utility.sharedInstance.rideStopped ?? false{ self.refProductController[1].rideViewModel.rideStatus = .past
                Utility.sharedInstance.rideStopped = false
            }
        }
        mainScrollView.setContentOffset(CGPoint(x: (CGFloat(selectedTap) * (self.view.frame.size.width)), y: 0), animated: true)
    }

    @IBAction func profileTap(_ sender: Any) {
        let profileVC = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
        self.navigationController?.pushViewController(profileVC ?? ProfileViewController(), animated: true)
        
    }
    
    
    
    //MARK:- Navigation Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? HomeRidesViewController {
            if segue.identifier == SegueIdentifier.upcomingRides {
               vc.selectedTabRideStatus = .upcoming
            } else if segue.identifier == SegueIdentifier.pastRides {
               vc.selectedTabRideStatus = .past
            }
            refProductController.append(vc)
            vc.rideViewModel.refreshOrdersClausre = { [weak self] in
                DispatchQueue.main.async {
                    if self?.refProductController.count == 2 {
                        if Utility.sharedInstance.rideStopped ?? false{ self?.refProductController[1].rideViewModel.rideStatus = .past
                        }
                        //self?.refProductController[1].rideViewModel.rideCompleted()
                    }
                }
            }
        }
    }
}
//MARK:- UIScrollViewDelegate
extension HomeViewController : UIScrollViewDelegate {
    func moveLine() {
        scrollLineLeadingConstraint.constant = mainScrollView.contentOffset.x/2
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.moveLine()
        if scrollView == mainScrollView {
            if scrollView.contentOffset.x <= scrollView.frame.size.width/2 {
                scrolltopTaps(topButtons[0])
            } else if scrollView.contentOffset.x <= (scrollView.frame.size.width + scrollView.frame.size.width/2) {
                scrolltopTaps(topButtons[1])
            }
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == mainScrollView {
            let xAxis = Int(scrollView.contentOffset.x/self.view.frame.size.width)
            topTaps(topButtons[xAxis])
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if scrollView == mainScrollView {
                let xAxis = Int(scrollView.contentOffset.x/self.view.frame.size.width)
                topTaps(topButtons[xAxis])
            }
        }
    }
    
    func scrolltopTaps(_ sender: UIButton) {
        self.selectedTap = sender.tag - 1  
    }
}
