//
//  RideDetailViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 27/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class RideDetailViewController: BaseViewController {

    //MARK:- Variables
    var rideDetail : Ride?
    var rideVM = RideViewModel()
    
    //MARK:- Clausures
    var rideStopped:((String)->())?
   
    
    //MARK:- IBOutlets
    @IBOutlet weak var mainScrollView: UIScrollView!{
        didSet{
            mainScrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        }
    }
    @IBOutlet weak var stackVw_infants: UIStackView!
    
    @IBOutlet weak var tbleVw_infants: UITableView!
    @IBOutlet weak var lbl_infantCount: SemiBoldLabel!
    @IBOutlet weak var carNameLabel : UILabel!
    @IBOutlet weak var pickUpLocationLabel : UILabel!
    @IBOutlet weak var dropLocationLabel : UILabel!
    @IBOutlet weak var dateTimeLabel : UILabel!
    @IBOutlet weak var customeNameLabel : UILabel!
    @IBOutlet weak var seatCapacityLabel : UILabel!
    @IBOutlet weak var luggageCapacityLabel : UILabel!
    @IBOutlet weak var customerPhoneLabel : UILabel!
    @IBOutlet weak var emailLabel : UILabel!
    @IBOutlet weak var startRideBtn : UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var startRideBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var startRideTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightConstrant_infantTable: NSLayoutConstraint!
    @IBOutlet weak var vw_scrollContent: UIView!
    
    @IBOutlet weak var topConstraint_table: NSLayoutConstraint!
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setRideVMClosures()
        if let infants =  rideDetail?.infants_childs{
            if infants.count > 0{
               heightConstrant_infantTable.constant = CGFloat(infants.count * 28) + CGFloat(75)
               topConstraint_table.constant = 10
            }else{
                 heightConstrant_infantTable.constant = 0
                 topConstraint_table.constant = 0
            }
        }else{
             heightConstrant_infantTable.constant = 0
             topConstraint_table.constant = 0
        }
        startRideBtn.isHidden = rideDetail?.isCompleted ?? false ? true : false
        startRideBtnHeight.constant = rideDetail?.isCompleted ?? false ? 0 : 45
       // startRideTopConstraint.constant = rideDetail?.isCompleted ?? false ? 0 : 20
        let rideStatus = rideDetail?.rideStatus
        startRideBtn.isSelected = rideStatus == "1" ? false : true
        let btnTitle = startRideBtn.isSelected ? "Stop Ride" : "Start Ride"
        startRideBtn.setTitle(btnTitle, for: .normal)
        carNameLabel.text = rideDetail?.carModel?.trim() == "" ? "N/A" : rideDetail?.carModel
        pickUpLocationLabel.text = rideDetail?.sourceLocation?.trim() == "" ? "N/A" : rideDetail?.sourceLocation
        dropLocationLabel.text = rideDetail?.destinationLocation?.trim() == "" ? "N/A" : rideDetail?.destinationLocation
        dateTimeLabel.text = rideDetail?.startDate?.trim() == "" ? "N/A" : rideDetail?.startDate
        customeNameLabel.text = rideDetail?.name?.trim() == "" ? "N/A" : rideDetail?.name
        seatCapacityLabel.text = rideDetail?.seatCount?.trim() == "" ? "N/A" : rideDetail?.seatCount
        luggageCapacityLabel.text = rideDetail?.luggageCount?.trim() == "" ? "N/A" : rideDetail?.luggageCount
        customerPhoneLabel.text = rideDetail?.riderNumber == nil ? "N/A" : rideDetail?.riderNumber
        emailLabel.text = rideDetail?.email == nil ? "N/A" : rideDetail?.email
        if let infants =  rideDetail?.infants_childs{
            if infants.count > 0{
                lbl_infantCount.text = "\(infants.count)"
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if getUser().profileImage != nil{
            profileImage.setImage(fromUrl: getUser().profileImage ?? "", defaultImage: UIImage(named: "Default_User"))
        }
    }
    

    //MARK:- IBAction
    
    @IBAction func action_callAdmin(_ sender: UIButton) {
        let number = "+442079934494"
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func profileTap(){
        let profileVC = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
        self.navigationController?.pushViewController(profileVC ?? ProfileViewController(), animated: true)
    }
    
    @IBAction func contactSupportTap(_ sender: Any) {
        let supportVC = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "ContactSupportViewController") as? ContactSupportViewController
        self.navigationController?.pushViewController(supportVC ?? ContactSupportViewController(), animated: true)
    }
    
    @IBAction func startRideTap(_ sender : UIButton){
        if Utility.sharedInstance.currentRide != nil && rideDetail?.rideStatus == "1"{
            self.showAlert(self, message:
                "You are already on a ride, so you cannot start another ride.")
        }else {
            sender.isSelected = !sender.isSelected
            let btnTitle = sender.isSelected ? "Stop Ride" : "Start Ride"
            sender.setTitle(btnTitle, for: .normal)
            self.rideDetail?.rideStatus = sender.isSelected ? "2" : "3"
            rideVM.ride = self.rideDetail
            rideVM.status = sender.isSelected ? "2" : "3"
           
        }
    }
    
    private func setRideVMClosures() {
      
        rideVM.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.rideVM.isLoading ?? false {
                    self?.showLoader(self)
                } else {
                    self?.hideLoader(self)
                }
            }
        }
        
        rideVM.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.rideVM.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        rideVM.responseReceived = { [weak self] () in
            DispatchQueue.main.async {
                if self?.rideVM.status == "3"{
                    Utility.sharedInstance.rideStopped = true
                    Utility.sharedInstance.currentRide = nil
                    self?.navigationController?.popViewController(animated: true)
                }else{
                    Utility.sharedInstance.currentRide = self?.rideDetail
                }
                self?.rideStopped?(self?.rideVM.status ?? "")
            }
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RideDetailViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        view.backgroundColor = UIColor.clear
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
        view.backgroundColor = UIColor.clear
        let label = UILabel.init(frame: CGRect.init(x: 10, y: 0, width: view.frame.size.width, height: 48))
        label.font = UIFont.init(name: "OpenSans-Bold", size: 14) ?? UIFont.systemFont(ofSize: 14)
        label.text = "Child Information"
        let view1 = UIView.init(frame: CGRect.init(x: 0, y: 48, width: tableView.frame.size.width, height: 1))
        view1.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.5)
        view.addSubview(label)
        view.addSubview(view1)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let infants =  rideDetail?.infants_childs{
            if infants.count > 0{
                return infants.count
            }
        }
         return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RideDetail_infantCell", for: indexPath) as? RideDetail_infantCell
        if let infants =  rideDetail?.infants_childs{
             cell?.lbl_childAge.text = infants[indexPath.row]
        }
        return cell ?? RideDetail_infantCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
}
