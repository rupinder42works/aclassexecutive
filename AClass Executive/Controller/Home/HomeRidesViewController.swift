//
//  HomeRidesViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 26/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class HomeRidesViewController: BaseViewController{

    // MARK: - Variables
    var selectedTabRideStatus: RideStatus = .all {
        didSet {
            setRideVMClosures()
            rideViewModel.rideStatus = selectedTabRideStatus
        }
    }
    var rideViewModel = RideViewModel()
    var refreshControl: UIRefreshControl!
    var setRefreshControl : Bool? {
        didSet {
            refreshControl = UIRefreshControl()
            refreshControl.backgroundColor = UIColor.clear
            refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            refreshControl.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
            ridesTableView.addSubview(self.refreshControl)
        }
    }
    var refreshClausre:(()->())?
    var page = 0
    var rideArr : [Ride]?

    
    // MARK: - IBOutlets
    @IBOutlet var ridesTableView : UITableView!{
        didSet {
            ridesTableView.register(UINib(nibName: "HomeRidesCell", bundle: nil), forCellReuseIdentifier: "HomeRidesCell")
            setRefreshControl = true
            ridesTableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        }
    }
    
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(selectedTabRideStatus)
    }
    
    // MARK: - Functions
    @objc private func refreshTable() {  
       rideViewModel.getRidesList(selectedTabRideStatus,true)
    }

    private func setRideVMClosures() {
       rideViewModel.responseReceived = { [weak self] in
            DispatchQueue.main.async {
                self?.rideArr = [Ride]()
                self?.rideArr = self?.rideViewModel.rideArr
                if Utility.sharedInstance.currentRide != nil{
                    for element in self?.rideArr ?? [Ride](){
                        if element.id == Utility.sharedInstance.currentRide?.id{
                            if let location = self?.rideArr?.index(where: { $0.id == element.id }), location < self?.rideArr?.count ?? 0 {
                                self?.rideArr?.remove(at: location)
                            }
                        }
                    }
                    self?.rideArr?.insert(Utility.sharedInstance.currentRide ?? Ride(), at: 0)
                }
                if self?.rideArr?.count == 0{
                    if self?.selectedTabRideStatus == .upcoming{
                      self?.showEmptyScreen(AppMessages.noUpcomingRide, superView: self?.ridesTableView)
                    }else{
                      self?.showEmptyScreen(AppMessages.noPastRide, superView: self?.ridesTableView)
                    }
                }else{
                    self?.removeEmptyMsg(superView: self?.ridesTableView)
                }
                
                
                self?.ridesTableView.reloadData()
            }
        }
        
        rideViewModel.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.rideViewModel.isLoading ?? false {
                    self?.showLoader(self)
                } else {
                    self?.hideLoader(self)
                }
            }
        }
        
        rideViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.rideViewModel.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        rideViewModel.endRefreshClausre = { [weak self] () in
            DispatchQueue.main.async {
                self?.refreshControl.endRefreshing()
            }
        }
        rideViewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.rideArr = self?.rideViewModel.rideArr
                if self?.rideArr?.count == 0{
                    if self?.selectedTabRideStatus == .upcoming{
                        self?.showEmptyScreen(AppMessages.noUpcomingRide, superView: self?.ridesTableView)
                    }else{
                        self?.showEmptyScreen(AppMessages.noPastRide, superView: self?.ridesTableView)
                    }
                    
                }else{
                    self?.removeEmptyMsg(superView: self?.ridesTableView)
                }
                self?.ridesTableView.reloadData()
            }
        }
    }
}

extension HomeRidesViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rideArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeRidesCell", for: indexPath) as? HomeRidesCell
        cell?.config(rideArr?[indexPath.row] ?? Ride())
        return cell ?? HomeRidesCell()
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rideDetailVC = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "RideDetailViewController") as? RideDetailViewController
        rideDetailVC?.rideDetail = self.rideArr?[indexPath.row]
       // rideDetailVC?.rideVM = self.rideViewModel
        rideDetailVC?.rideStopped = {status in
          
           self.rideViewModel.getRidesList(.upcoming, true)
//          DispatchQueue.main.async {
//            if status == "3"{
//                if indexPath.row < self.rideViewModel.rideArr?.count ?? 0{
//                    self.rideViewModel.rideArr?.remove(at: indexPath.row)
//                    self.rideViewModel.reloadTableViewClosure?()
//                }
//            }else{
//                self.rideViewModel.getRidesList(.upcoming, true)
//            }
//          }
        }
        self.navigationController?.pushViewController(rideDetailVC ?? RideDetailViewController(), animated: true)
    }
    
}

//MARK:- UIScrollViewDelegate
extension HomeRidesViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y + scrollView.frame.size.height >= scrollView.contentSize.height {
            self.loadMore()
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if scrollView.contentOffset.y + scrollView.frame.size.height >= scrollView.contentSize.height {
                self.loadMore()
            }
        }
    }
    
    func loadMore() {
        if rideViewModel.isLoadMoreRequired {
            page = rideViewModel.page
            if page != Int((rideViewModel.rideArr?.count ?? 0/20) + 1){
                self.page = Int((rideViewModel.rideArr?.count ?? 0/20) + 1)
                self.rideViewModel.getRidesList(selectedTabRideStatus,false)
            }else{
                return
            }
        }
    }
}
