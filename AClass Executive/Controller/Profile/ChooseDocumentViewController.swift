//
//  ChooseDocumentViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 10/01/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import UIKit

class ChooseDocumentViewController: BaseViewController {
    
    //MARK:- Variables
    var documentName : String?
    var radioBtnTag : Int?
    var photoManager:PhotoManager?
    var documentVM = DocumentViewModel()
    var documnetTypeSelected : Bool?{
        didSet{
            if documnetTypeSelected ?? false{
                self.dismiss(animated: false, completion: nil)
                documentTypeSelected?()
            }
        }
    }
    
    var docCategoriesArr : [String]? {
        didSet {
            docCategoriesArr = [String]()
            docCategoriesArr?.append("PCO Driver Licence(Paper Part)")
            docCategoriesArr?.append("PCO Driver Badge")
            docCategoriesArr?.append("PCO Car License")
            docCategoriesArr?.append("MOT")
            docCategoriesArr?.append("Car Insurance")
            docCategoriesArr?.append("V5 Document")
            docCategoriesArr?.append("Passport size photo")
            docCategoriesArr?.append("Car Picture - Front with visible Car Reg")
            tbleVw_doc.reloadData()
        }
    }
    
    //MARK:- IBOutlets
    @IBOutlet var radioBnCollection: [UIImageView]!
    @IBOutlet var chooseDocumentNameView: UIView!
    @IBOutlet var docTypeView: UIView!
    @IBOutlet var typeNameView: UIView!
    @IBOutlet var documentNameTF : UITextField!
    
    var selectedIndexPath = [Int]()
    var selectedRadios = [Bool]()
    
    @IBOutlet weak var tbleVw_doc: UITableView!
    
    //MARK:- Clausers
    var documentTypeSelected:(()->())?
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tbleVw_doc.delegate = self
        tbleVw_doc.dataSource = self
        tbleVw_doc.tableFooterView = UIView()
        tbleVw_doc.isHidden = true
        chooseDocumentNameView.isHidden = true
        docTypeView.isHidden = false
        
//        tbleVw_doc.estimatedRowHeight = 70
//        tbleVw_doc.rowHeight = UITableView.automaticDimension
       // docCategoriesArr = [String]()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    // MARK: - IBAction
    
    @IBAction func radioBtnSelection(_ sender : UIControl){
        for radiobtn in radioBnCollection{
            if radiobtn.tag == sender.tag{
                radioBtnTag = radiobtn.tag
                radiobtn.isHighlighted = true
                documentName = getReason(radiobtn.tag)
                if radiobtn.tag == 6{
                    typeNameView.isHidden = false
                }else{
                    typeNameView.isHidden = true
                }
            }else{
                radiobtn.isHighlighted = false
            }
        }
    }
    
    @IBAction func nextBtnTap(){
        if documentName?.trim() == "" || documentName == nil{
            if  radioBtnTag != 6{
                showAlert(self, message: AppMessages.noDocumentNameSelected)
                return
            }else{
                showAlert(self, message: AppMessages.noDocumnetNameEntered)
                return
            }
        }
        
        documentVM.docName = documentName?.trim()
        chooseDocumentNameView.isHidden = true
        docTypeView.isHidden = false
        
    }
    
    @IBAction func imageTypeSelected(){
        documentVM.docType = .Image
        documnetTypeSelected = true
    }
    
    @IBAction func pdfTypeSelected(){
        documentVM.docType = .Pdf
        documnetTypeSelected = true
    }
    
    @IBAction func dismissBtnTap(){
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    // MARK: - Function
    
    func getReason(_ index : Int) -> String{
        var documentName : String?
        switch index {
        case 1:
            documentName = "driver_licence"
            break
        case 2:
            documentName = "driver_badge"
            break
        case 3:
            documentName = "car_licence"
            break
        case 4:
            documentName = "mot"
            break
        case 5:
            documentName = "car_insurance"
            break
        case 6:
            documentName = "v5document"
            break
        case 7:
            documentName = "driver_picture"
            break
        case 8:
            documentName = "car_picture"
            break
        default:
            break
        }
        return documentName ?? ""
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension ChooseDocumentViewController : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        documentName = textField.text?.trim()
    }
}

extension ChooseDocumentViewController : UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let cico = url as URL
        print(cico)
        print(url)
        
        print(url.lastPathComponent)
        
        print(url.pathExtension)
        
    }
}

extension ChooseDocumentViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arr = docCategoriesArr{
            return arr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDocument_TableCell", for: indexPath) as? SelectDocument_TableCell
        if let arr = docCategoriesArr{
            cell?.lbl_docTitle.text = arr[indexPath.row]
        }
        
        if selectedIndexPath.contains(indexPath.row) == true{
            var index : Int?
            for i in 0..<selectedIndexPath.count{
                if selectedIndexPath[i] == indexPath.row{
                    index = i
                    break
                }
            }
            if index != nil{
                if selectedRadios[index!] == true{
                    cell?.imgVw_radio.image = #imageLiteral(resourceName: "radioBtnSelected")
                }else{
                    cell?.imgVw_radio.image = #imageLiteral(resourceName: "radioBtnUnselected")
                }
            }else{
                cell?.imgVw_radio.image = #imageLiteral(resourceName: "radioBtnUnselected")
            }
        }else{
            cell?.imgVw_radio.image = #imageLiteral(resourceName: "radioBtnUnselected")
        }
        return cell ?? SelectDocument_TableCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var index : Int?
        for i in 0..<selectedIndexPath.count{
            if selectedIndexPath[i] == indexPath.row{
                index = i
                break
            }
        }
        if index != nil{
            if selectedRadios[index!] == true{
                selectedRadios.remove(at: index!)
                selectedIndexPath.remove(at: index!)
                documentName = ""
                
            }else{
                selectedRadios.removeAll()
                selectedIndexPath.removeAll()
                self.selectedIndexPath.append(indexPath.row)
                self.selectedRadios.append(true)
                documentName = getReason(indexPath.row + 1)
            }
        }else{
            selectedRadios.removeAll()
            selectedIndexPath.removeAll()
            self.selectedIndexPath.append(indexPath.row)
            self.selectedRadios.append(true)
            documentName = getReason(indexPath.row + 1)
        }
        tableView.reloadData()
    }
    
}

