//
//  ProfileViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 27/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    var docCategoriesArr : [String]? {
        didSet {
            docCategoriesArr = [String]()
            docCategoriesArr?.append("PCO Driver Licence(Paper Part)")
            docCategoriesArr?.append("PCO Driver Badge")
            docCategoriesArr?.append("PCO Car License")
            docCategoriesArr?.append("MOT")
            docCategoriesArr?.append("Car Insurance")
            docCategoriesArr?.append("V5 Document")
            docCategoriesArr?.append("Passport size photo")
            docCategoriesArr?.append("Car Picture - Front with visible Car Reg")
        }
    }
    
    @IBOutlet weak var htConstrnt_scrollView: NSLayoutConstraint!
    //MARK:- Variables
    var changePwdViewModel: ChangePwdViewModel?
    var profileViewModel = ProfileViewModel()
    var photoManager:PhotoManager?
    var documentVM = DocumentViewModel()
    var userDocs = [Document]()
    // MARK: - IBOutlets
    @IBOutlet weak var profileImage : UIImageView!
    @IBOutlet weak var updatePasswordStackView: UIStackView!
    @IBOutlet weak var showPasswordStackView: UIStackView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stackViewBottom: NSLayoutConstraint!
    @IBOutlet weak var phoneStackView : UIStackView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ridesCompletedLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumLabel: UILabel!
    @IBOutlet weak var textFieldOldPwd: UITextField!
    @IBOutlet weak var textFieldNewPwd: UITextField!
    @IBOutlet weak var textFieldConfirmPwd: UITextField!
    @IBOutlet weak var descriptionStackView: UIStackView!
    @IBOutlet weak var mainScrollView: UIScrollView!{
        didSet{
            mainScrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        }
    }
    @IBOutlet weak var documentsCollectionView: UICollectionView!{
        didSet{
            documentsCollectionView.delegate = self
            documentsCollectionView.dataSource = self
            documentsCollectionView.register(UINib(nibName: "DocumnetsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "DocumnetsCollectionCell")
        }
    }
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseVM()
        docCategoriesArr = [String]()
        intializeDocumentModel()
        mainScrollView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    func intializeDocumentModel(){
        for i in 0..<7{
            var dm = Document()
            if i == 0{
                dm.docType = "driver_licence"
            }else  if i == 1{
                dm.docType = "driver_badge"
            }else  if i == 2{
                dm.docType = "car_licence"
            }else  if i == 3{
                dm.docType = "mot"
            }
            else  if i == 4{
                dm.docType = "v5document"
            }
            else  if i == 5{
                dm.docType = "driver_picture"
            }
            else  if i == 6{
                dm.docType = "car_picture"
            }
            userDocs.append(dm)
            
        }
    }
    
    //MARK:- Private Methods
    private func initialiseVM() {
        profileViewModel.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.profileViewModel.isLoading ?? false {
                    self?.showLoader(self)
                } else {
                    self?.hideLoader(self)
                }
            }
        }
        
        profileViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.profileViewModel.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        profileViewModel.userProfileRetrieved = { [weak self] () in
            DispatchQueue.main.async {
                self?.handleResponse()
                if let documents = self?.profileViewModel.userDocuments{
                    for values in documents{
                        var count = 0
                        if let docs = self?.userDocs{
                            count = docs.count
                        }
                        for i in 0..<count{
                            if self?.userDocs[i].docType == values.name{
                                self?.userDocs[i] = values
                                break
                            }
                        }
                    }
                }
                
                self?.documentsCollectionView.reloadData()
            }
        }
        
        profileViewModel.retrieveUserProfile = true
    }
    
    //MARK:- Function
    
    private func handleResponse(){
        let user = getUser()
        mainScrollView.isHidden = false
        profileImage.setImage(fromUrl: user.profileImage ?? "", defaultImage: UIImage(named: "Default_User"))
        self.nameLabel.text = user.name ?? ""
        let ridesCompleted = user.rides ?? 0
        self.ridesCompletedLabel.text = ridesCompleted == 0 ? "0 rides completed" : "\(ridesCompleted) rides completed"
        self.emailLabel.text = user.email ?? ""
        self.descriptionLabel.text = user.bio ?? ""
        self.descriptionStackView.isHidden = user.bio == "" ? true : false
        self.phoneNumLabel.text = user.phone == "" ? "N/A" : user.phone ?? ""
    }
    
    
    // MARK: - IBAction
    @IBAction func logoutTap(_ sender: Any) {
        let logoutView = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "LogoutViewController") as? LogoutViewController
        logoutView?.modalPresentationStyle = .overFullScreen
        (AppDel.window?.rootViewController as? UINavigationController)?.present(logoutView ?? LogoutViewController(), animated: false, completion: nil)
    }
    
    @IBAction func changePasswordTap(_ sender: Any) {
        showPasswordStackView.isHidden = true
        updatePasswordStackView.isHidden = false
    }
    
    @IBAction func updatePasswordTap(_ sender: Any) {
        changePwdViewModel = ChangePwdViewModel(oldPwd: textFieldOldPwd.text ?? "", newPwd: textFieldNewPwd.text ?? "", confirmPwd: textFieldConfirmPwd.text ?? "")
        
        changePwdViewModel?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.changePwdViewModel?.isLoading ?? false {
                    self?.showLoader(self)
                } else {
                    self?.hideLoader(self)
                }
            }
        }
        
        changePwdViewModel?.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.changePwdViewModel?.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        changePwdViewModel?.passwordChanged = {[weak self] () in
            DispatchQueue.main.async {
                self?.showPasswordStackView.isHidden = false
                self?.updatePasswordStackView.isHidden = true
            }
        }
        
        changePwdViewModel?.pwdChange()
    }
    
    @IBAction func editProfileTap(_ sender: Any) {
        let editProfileVC = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController
        editProfileVC?.profileUpdated = { [weak self] () in
            self?.handleResponse()
        }
        self.navigationController?.pushViewController(editProfileVC ?? EditProfileViewController(), animated: true)
    }
    
    @IBAction func readMoreBtnTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        descriptionLabel.numberOfLines = sender.isSelected ? 0 : 2
    }
    
    @IBAction func uploadImageTap() {
        photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, callback: { (pickedImage) in
            self.profileImage.image = pickedImage ?? UIImage()
            self.profileViewModel.profileImage = pickedImage
            self.profileViewModel.uploadUserProfilePicture()
        })
    }
    
    @IBAction func openChooseDocumentPopUp(){
        let chooseDocument = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "ChooseDocumentViewController") as? ChooseDocumentViewController
        chooseDocument?.modalPresentationStyle = .overFullScreen
        
        
        chooseDocument?.documentTypeSelected = {[weak self] () in
            self?.documentVM = chooseDocument?.documentVM ?? DocumentViewModel()
            self?.documentVM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if self?.documentVM.isLoading ?? false {
                        self?.showLoader(self)
                    } else {
                        self?.hideLoader(self)
                    }
                }
            }
            self?.documentVM.showAlertClosure = { [weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.documentVM.alertMessage {
                        self?.showAlert(self, message: message)
                    }
                }
            }
            self?.documentVM.newDocumentArr = { arr in
                    for values in arr{
                        var count = 0
                        if let docs = self?.userDocs{
                            count = docs.count
                        }
                        for i in 0..<count{
                            if self?.userDocs[i].docType == values.name{
                                self?.userDocs[i] = values
                                break
                            }
                        }
                    }
                //self?.profileViewModel.userDocuments = arr
                self?.documentsCollectionView.reloadData()
            }
            
            if self?.documentVM.docType == .Image {
                self?.photoManager = PhotoManager(navigationController: self?.navigationController ?? UINavigationController(), allowEditing: true, callback: { (pickedImage) in
                    self?.documentVM.selectedImage = pickedImage
                })
            } else {
                let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
                documentPicker.delegate = self
                self?.navigationController?.present(documentPicker, animated: true, completion: nil)
            }
        }
        
        self.present(chooseDocument ?? ChooseDocumentViewController(), animated: false, completion: nil)
        
    }
    
    func selectImage(tag:Int){
        
        self.documentVM =  DocumentViewModel()
        if let docArr = self.docCategoriesArr{
            self.documentVM.docName = docArr[tag]
        }
        self.documentVM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if self.documentVM.isLoading ?? false {
                    self.showLoader(self)
                } else {
                    self.hideLoader(self)
                }
            }
        }
        self.documentVM.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.documentVM.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        self.documentVM.newDocumentArr = { arr in
           // self.profileViewModel.userDocuments = arr
            for values in arr{
                var count = 0
                 //let docs = self.userDocs{
                    count = self.userDocs.count
                //}
                
                for i in 0..<count{
                    if self.userDocs[i].docType == values.docType{
                        self.userDocs[i] = values
                        break
                    }
                }
            }
            self.documentsCollectionView.reloadData()
        }
        
        if self.documentVM.docType == .Image {
            self.photoManager = PhotoManager(navigationController: self.navigationController ?? UINavigationController(), allowEditing: true, callback: { (pickedImage) in
                self.documentVM.selectedImage = pickedImage
            })
        } else {
            let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
            documentPicker.delegate = self
            self.navigationController?.present(documentPicker, animated: true, completion: nil)
        }
        
    }
}

extension ProfileViewController : UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        self.documentVM.selectedPdfUrl = cico
    }
}

//MARK:- UICollectionView Delegate & DataSource

extension ProfileViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
           return userDocs.count
        
        //return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumnetsCollectionCell", for: indexPath) as? DocumnetsCollectionCell else {
            return UICollectionViewCell()
        }
        collectionView.layoutIfNeeded()
        htConstrnt_scrollView.constant = collectionView.contentSize.height + 380
        mainScrollView.layoutIfNeeded()
//        if userDocs == nil || userDocs.count == indexPath.row {
//            cell.isAddCell = true
//        } else {
//            cell.isAddCell = false
//        }
        
        cell.addNewDocument = { [weak self] () in
            self?.openChooseDocumentPopUp()
           // self?.selectImage(tag:indexPath.row)
        }
        if let docArr = docCategoriesArr{
            cell.lbl_imagename.text = docArr[indexPath.row]
        }else{
            cell.lbl_imagename.text = ""
        }
        
                let obj = userDocs[indexPath.row]
                   // if cell.lbl_imagename.text?.contains(textTrim) == true{
                        cell.config(obj)
                  //  }
                
        
        
        cell.removeDocument = {
            self.profileViewModel.removeDocument(indexPath.row, complete: { status in
                if status {
                    self.documentsCollectionView.reloadData()
                }
            })
        }
        return cell
    }
}

//MARK:- CollectionView Layout
extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width - 40, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //onTapImage?(Int(indexPath.row))
    }
}

//MARK:- UITextFieldDelegate
extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldOldPwd {
            textFieldNewPwd.becomeFirstResponder()
        } else if textField == textFieldNewPwd {
            textFieldConfirmPwd.becomeFirstResponder()
        } else if textField == textFieldConfirmPwd {
            textFieldConfirmPwd.resignFirstResponder()
        }
        return true
    }
}
