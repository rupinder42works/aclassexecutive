//
//  LogoutViewController.swift
//  Magic
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LogoutViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func crossBtnTap(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func logoutBtnTap(_ sender: Any) {
        let profileViewModel = ProfileViewModel()
        profileViewModel.isLogout = true
        profileViewModel.logoutSuccess = {
            DispatchQueue.main.async {
                UserDefaults.standard.removeObject(forKey: UserDefault.user)
                UserDefaults.standard.synchronize()
                AppDel.logoutTap()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
