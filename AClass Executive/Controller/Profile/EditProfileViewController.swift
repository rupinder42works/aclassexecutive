//
//  EditProfileViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 27/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController {

    //MARK:- Variables
    var editProfileVM : EditProfileViewModel?
    
    //MARK:- Clausures
    var profileUpdated:(()->())?

    //MARK:- IBOutlets
    @IBOutlet var emailTextField : UITextField!
    @IBOutlet var nameTextField : UITextField!
    @IBOutlet var phoneTextField : UITextField!
    @IBOutlet var bioTextView : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.isUserInteractionEnabled = false
        emailTextField.text = self.getUser().email
        nameTextField.text = self.getUser().name
        phoneTextField.text = self.getUser().phone
        bioTextView.text = self.getUser().bio
        // Do any additional setup after loading the view.
    }
    

    
    //MARK:- IBAction
    @IBAction func updateProfileTap(){
        editProfileVM = EditProfileViewModel()
        editProfileVM?.email = emailTextField.text?.trim() ?? ""
        editProfileVM?.name = nameTextField.text?.trim() ?? ""
        editProfileVM?.phone = phoneTextField.text?.trim() ?? ""
        editProfileVM?.bio = bioTextView.text?.trim() ?? ""
        
        
        editProfileVM?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.editProfileVM?.isLoading ?? false {
                    self?.showLoader(self)
                } else {
                    self?.hideLoader(self)
                }
            }
        }
        
        editProfileVM?.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.editProfileVM?.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        editProfileVM?.editProfileSuccess = { [weak self] () in
            self?.profileUpdated?()
            self?.navigationController?.popViewController(animated: true)
        }
        
        editProfileVM?.sessionExpired = { [weak self] () in
            
        }
        
        editProfileVM?.editProfile()
 
    }
}
extension EditProfileViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            phoneTextField.becomeFirstResponder()
        }else if textField == phoneTextField {
            bioTextView.becomeFirstResponder()
        }else {
            view.endEditing(true)
        }
        return true
    }
}
extension EditProfileViewController : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
