//
//  ForgetPasswordViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 26/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController {

    //Mark:- IBOutlets
    @IBOutlet var emailTxtField : UITextField!
    
    //Mark:- Variables
    var forgetPwdViewModel : ForgetPasswordViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- IBAction
    @IBAction func forgotPwdTap() {
        let forgotManager = ForgotManager()
        forgotManager.email = emailTxtField.text?.trim() ?? ""
        
        let status = forgotManager.isFormValid().0
        let message = forgotManager.isFormValid().1
    
        forgetPwdViewModel = ForgetPasswordViewModel()

        if !status {
            self.showAlert(self, message: message)
            return
        }
        
        
        forgetPwdViewModel?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.forgetPwdViewModel?.isLoading ?? false {
                    self?.showLoader(self)
                } else {
                    self?.hideLoader(self)
                }
            }
        }
        
        forgetPwdViewModel?.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.forgetPwdViewModel?.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        forgetPwdViewModel?.emailSentSuccess = { [weak self] () in
            DispatchQueue.main.async {
                if let response = self?.forgetPwdViewModel?.responseSuccess {
                    print(response)
                    
                }
            }
        }
        
         self.view.endEditing(true)
         forgetPwdViewModel?.validateData(forgotManager)

    }

}
