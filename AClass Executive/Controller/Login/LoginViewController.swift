//
//  LoginViewController.swift
//  AClass Executive
//
//  Created by Apple1 on 26/12/2018.
//  Copyright © 2018 Apple1. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var emailTF : UITextField!
    @IBOutlet var passwordTF : UITextField!
    
    //MARK:- Variables
    var loginViewModel : LoginViewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - IBAction

 
    //MARK:- IBActions
    @IBAction func loginTap(_ sender : UIButton) {
        
        let loginModel = LoginModel()
        loginModel.email = emailTF.text?.trim() ?? ""
        loginModel.pwd = passwordTF.text?.trim() ?? ""
        
        let status = loginModel.isFormValid().0
        let message = loginModel.isFormValid().1
        
        
        if !status {
            self.showAlert(self, message: message)
            return
        }
        
        
        loginViewModel.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.loginViewModel.isLoading ?? false {
                    self?.showLoader(self)
                } else {
                    self?.hideLoader(self)
                }
            }
        }
        
        loginViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.loginViewModel.alertMessage {
                    self?.showAlert(self, message: message)
                }
            }
        }
        
        loginViewModel.loginSuccess = { [weak self] () in
            DispatchQueue.main.async {
                if let response = self?.loginViewModel.responseSuccess {
                    print(response)
                    let homeVC = UIStoryboard(name: StoryboardName.home, bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                    self?.navigationController?.pushViewController(homeVC ?? HomeViewController(), animated: true)
                }
            }
        }
        
        if status {
            //Service call
            self.view.endEditing(true)
            loginViewModel.validateData(loginModel)
        } else {
            //Alert
            self.showAlert(self, message: message)
        }
    }
    

}
//MARK:- UITextFieldDelegate
extension LoginViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTF {
            passwordTF.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
}
