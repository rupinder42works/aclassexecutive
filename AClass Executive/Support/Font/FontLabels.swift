//
//  FontLabels.swift
//  PetConnect
//
//  Created by 42Works-Worksys2 on 21/09/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Foundation

//MARK:- Label classes
class BoldLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "OpenSans-Bold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
    }
}

class SemiBoldLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "OpenSans-SemiBold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
    }
}

class RobotoLightLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Light", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
    }
}

class RegularLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "OpenSans-Regular", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
    }
}


//MARK:- Button classes
class BoldButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "OpenSans-Bold", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

class SemiBoldButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "OpenSans-SemiBold", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

class LightButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "Roboto-Light", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

//MARK:- Textfield classes


class RobotoTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Light", size: 14) ?? UIFont.systemFont(ofSize: 14)
        self.textColor = AppColor.darkGrayColor
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: AppColor.lightGrayColor])
    }
}

class SemiBoldTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "OpenSans-SemiBold", size: 14) ?? UIFont.systemFont(ofSize: 14)
        self.textColor = AppColor.darkGrayColor
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: AppColor.lightGrayColor])
    }
}
