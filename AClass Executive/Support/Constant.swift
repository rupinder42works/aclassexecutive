//
//  Constant.swift
//  GoDolly
//
//  Created by 42Works-Worksys2 on 14/05/18.
//  Copyright © 2018 42works. All rights reserved.
//

import UIKit
import Foundation

struct ServerUrl {
    //static let link = "https://aclassexe.wpengine.com/wp-json/wp/v2/"
    
    // live url
    static let link = "https://aclassexecutives.co.uk/wp-json/wp/v2/"
}

struct AppNotification {
    static let received = "notRecieved"
}

struct ServiceName {
    static let login = "user/login"
    static let forgotPwd = "user/password/forgot"
    static let changePwd = "user/password/reset"
    static let logout = "users/logout"
    static let viewProfile = "user/detail"
    static let editProfile = "account/edit"
    static let updateProfilePicture = "profile/image/update"
    static let contactSupport = "contactsupport"
    static let homeListng = "rides/listing"
    static let updateRideStatus = "update/ride"
    static let documentUpload = "document/upload"
    static let updateLocation = "update/location"
    static let removeDocument = "document/remove"
}

struct SegueIdentifier {
    static let upcomingRides = "UpcomingRides"
    static let pastRides = "PastRides"
}

struct AppColor {
    static let primaryColor = UIColor(red: 183.0/255.0, green: 145.0/255.0, blue: 98.0/255.0, alpha: 1.0)
    static let dividerColor = UIColor(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
    static let lightGrayColor = UIColor(red: 184.0/255.0, green: 197.0/255.0, blue: 201.0/255.0, alpha: 1.0)
    static let darkGrayColor = UIColor(red: 100/255.0, green: 100/255.0, blue: 100/255.0, alpha: 1.0)
    static let darkTextColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
}

struct StoryboardName {
    static let home = "Home"
    static let login = "Login"
    static let setting = "Settings"
}

struct ValidationMessage {
    static let emptyEmail = "Please enter email address."
    static let emptyPwd = "Please enter password."
    static let emptyName = "Please enter your name."
    static let emptyPhone = "Please enter your phone number."
    static let emptyBio = "Please enter you bio."
    static let firstName = "Please enter first name."
    static let lastName = "Please enter your last name."
    static let subject = "Please enter your subject for contacting us."
    static let message = "Please enter you message for contacting us."
    static let invalidEmail = "Please enter valid email address."
    static let invalidPhone = "Please enter valid phone number."
    static let emptyOldPwd = "Please enter old password."
    static let emptyNewPwd = "Please enter new passowrd."
    static let emptyConfirmPwd = "Please enter confirm password."
    static let confirmPwdNoMatch = "New password and confirm password do not match."
}

struct AppMessages {
    static let noDocumentNameSelected = "Please select document Name."
    static let noDocumnetNameEntered = "Please select document Name."
    static let noInternetConnection = NSLocalizedString("No internet connection. Please try again.", comment: "")
    static let logout = NSLocalizedString("You have been logged out. Please login to continue.", comment: "")
    static let sessionExpired = "Your session has expired, please login to continue."
    static let noNumber = "Number not available"
    static let noUpcomingRide = "No upcoming ride is available."
    static let noPastRide = "No past rides!"
}

struct UserDefault {
    static let user = "user"
    static let deviceId = "deviceId"
    static let selectedLanguage = "selectedLanguage"
    static let appleLanguagesKey = "AppleLanguages"
}
