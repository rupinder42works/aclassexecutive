////
////  WebService.swift
////  Magic
////
////  Created by Mac on 28/05/18.
////  Copyright © 2018 42works. All rights reserved.
////

import Foundation
import Alamofire


typealias ServiceResponseReturn = (Bool?, String?, NSDictionary?) -> Void
typealias ServiceResponseString = (Bool?, String?) -> Void

class WebServices {

    func isInternetWorking() -> (Bool) {
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            return true
        }else{
            print("Internet Connection not Available!")
            return false
        }
    }
    
    func getUserProfile(methodName:String,params:NSDictionary?,oncompletion:@escaping ServiceResponseReturn){
        if !isInternetWorking() {
            oncompletion(false,AppMessages.noInternetConnection,nil)
            return
        }
        var request = URLRequest(url: URL(string: ServerUrl.link + methodName)!)
        request.httpMethod = "GET"
        
        if params != nil {
            request.httpBody = try! JSONSerialization.data(withJSONObject: params ?? NSDictionary())
        }
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        
        Alamofire.request(request)
            .responseJSON(queue: queue) { response in
                if response.result.value != nil {
                    if let response = response.result.value as? [String : Any] {
                        print(response)
                        let success = response["status"] as? Bool
                        let message = response["message"] as? String
                        oncompletion(success,message,response as NSDictionary)
                    }
                } else {
                    oncompletion(false, "Server error occured", nil)
                }
        }
    }
    
    func loginMedia(methodName:String, params:NSDictionary, oncompletion:@escaping ServiceResponseReturn) {
        
        if !isInternetWorking() {
            oncompletion(false,AppMessages.noInternetConnection,nil)
            return
        }
        
        var request = URLRequest(url: URL(string: ServerUrl.link + methodName)!)
        print(request)
        
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: params)
        
        Alamofire.request(request).responseJSON { (response) in
            if response.result.value != nil {
                if let response = response.result.value as? [String : Any] {
                    print(response)
                    let success = response["status"] as? Bool
                    let message = response["message"] as? String
                    oncompletion(success,message,response as NSDictionary)
                }
            } else {
                oncompletion(false, "Server error occured", nil)
            }
        }
    }
    

    func postRequest(methodName:String, params:NSDictionary?, oncompletion:@escaping ServiceResponseReturn) {
        if !isInternetWorking() {
            oncompletion(false,AppMessages.noInternetConnection,nil)
            return
        }

        var request = URLRequest(url: URL(string: ServerUrl.link + methodName)!)
        print(request)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: params ?? NSDictionary())
        
        Alamofire.request(request).responseJSON { (response) in
            if response.result.value != nil {
                if let response = response.result.value as? [String : Any] {
                    print(response)
                    let success = response["status"] as? Bool
                    let message = response["message"] as? String
                    oncompletion(success,message,response as NSDictionary)
                }
            } else {
                oncompletion(false, "Server error occured", nil)
            }
        }
    }
    
    func getRequest(methodName:String, params:NSDictionary?, oncompletion:@escaping ServiceResponseReturn) {
        if !isInternetWorking() {
            oncompletion(false,AppMessages.noInternetConnection,nil)
            return
        }
        var request = URLRequest(url: URL(string: ServerUrl.link + methodName)!)
        print(request)
       
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if params != nil {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params!)
        }
        
        Alamofire.request(request).responseJSON { (response) in
            if response.result.value != nil {
                if let response = response.result.value as? [String : Any] {
                    print(response)
                    let success = response["status"] as? Bool
                    let message = response["message"] as? String
                    oncompletion(success,message,response as NSDictionary)
                }
            } else {
                oncompletion(false, "Server error occured", nil)
            }
        }
    }
    
    func postMainRequest(methodName:String, params:NSDictionary?, oncompletion:@escaping ServiceResponseReturn) {
        if !isInternetWorking() {
            oncompletion(false,AppMessages.noInternetConnection,nil)
            return
        }
        var request = URLRequest(url: URL(string: ServerUrl.link + methodName)!)
        print(request)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpBody = try! JSONSerialization.data(withJSONObject: params ?? NSDictionary())
        
        Alamofire.request(request).responseJSON { (response) in
            if response.result.value != nil {
                if let response = response.result.value as? [String : Any] {
                    print(response)
                    let success = response["status"] as? Bool
                    let message = response["message"] as? String
                    oncompletion(success,message,response as NSDictionary)
                }
            } else {
                oncompletion(false, "Server error occured", nil)
            }
        }
    }
    
    func uploadImage(methodName:String, params:NSDictionary?, oncompletion:@escaping ServiceResponseReturn) {
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        
        upload(multipartFormData: { (multipartFormData) in
            if let data = params?.value(forKey: "file") as? Data {
                multipartFormData.append(data, withName: "file", fileName: "path.png", mimeType: "image/jpeg")
            }
            
            if let params = params {
                for (key, value) in params {
                    if let value = value as? String {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as? String ?? "")
                    }
                }
            }
        },
               usingThreshold: UInt64.init(),
               to: ServerUrl.link + methodName,
               method: .post,
               headers: header
            
        ) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response)
                    if response.result.value != nil {
                        if let response = response.result.value as? [String : Any] {
                            print(response)
                            let success = response["status"] as? Bool
                            let message = response["message"] as? String
                            oncompletion(success,message,response as NSDictionary)
                        }
                    } else {
                        oncompletion(false, "Server error occured", nil)
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                oncompletion(false, encodingError.localizedDescription, nil)
            }
        }
    }
    
    func uploadDocument(methodName:String, params:NSDictionary?, oncompletion:@escaping ServiceResponseReturn) {
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        
        upload(multipartFormData: { (multipartFormData) in
            if let dataType = params?.value(forKey: "type") as? String{
                if dataType == "Pdf"{
                    if let data = params?.value(forKey: "file") as? Data {
                        multipartFormData.append(data, withName: "file", fileName: "path.pdf", mimeType: "application/pdf")
                    }
                }else{
                    if let data = params?.value(forKey: "file") as? Data {
                        multipartFormData.append(data, withName: "file", fileName: "path.png", mimeType: "image/jpeg")
                    }
                }
            }
            if let params = params {
                for (key, value) in params {
                    if let value = value as? String {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as? String ?? "")
                    }
                }
            }
        },
               usingThreshold: UInt64.init(),
               to: ServerUrl.link + methodName,
               method: .post,
               headers: header
            
        ) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response)
                    if response.result.value != nil {
                        if let response = response.result.value as? [String : Any] {
                            print(response)
                            let success = response["status"] as? Bool
                            let message = response["message"] as? String
                            oncompletion(success,message,response as NSDictionary)
                        }
                    } else {
                        oncompletion(false, "Server error occured", nil)
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                oncompletion(false, encodingError.localizedDescription, nil)
            }
        }
    }
}

