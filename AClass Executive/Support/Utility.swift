//
//  Utility.swift
//  Magic
//
//  Created by Apple1 on 21/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import GoogleMaps

class Utility : NSObject{
    
    static let sharedInstance = Utility()
    var currentUserId : Int = 0
    var currentUserEmail : String?
    var isAddScreen = true
    var currentLocation: CLLocation?
    let locationManager = CLLocationManager()
    var currentRide : Ride?
    var rideStopped : Bool?

    private override init() {
        super.init()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingHeading()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.distanceFilter = 100.0
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func getUserData() -> User {
        if let userData = UserDefaults.standard.object(forKey: UserDefault.user) as? Data {
            let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? User ?? User()
            return user
        }
        return User()
    }
    
    //MARK:- Update Location Service
    
    func updateLocation(_ location: CLLocation?) {
        if location?.coordinate.latitude == self.currentLocation?.coordinate.latitude && location?.coordinate.longitude == self.currentLocation?.coordinate.longitude {
            return
        }
        
        if Utility.sharedInstance.currentUserEmail == nil || location == nil {
            return
        }
    
        let dic = ["user_email": Utility.sharedInstance.currentUserEmail ?? "",
                   "device_id" : UserDefaults.standard.value(forKey: UserDefault.deviceId) as? String ?? "",
                   "driver_location": "\(location?.coordinate.latitude ?? 0.0),\(location?.coordinate.longitude ?? 0.0)"] as [String : Any]
        
        WebServices().postMainRequest(methodName: ServiceName.updateLocation, params: dic as NSDictionary , oncompletion: { (status, message, response) in
            
            if let response = response {
                print(response)
            }
        })
    }
    
    func logoutUser(){
        DispatchQueue.main.async {
            UserDefaults.standard.removeObject(forKey: UserDefault.user)
            UserDefaults.standard.synchronize()
            AppDel.sessionExpired()
        }
    }
    
}
extension Utility: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DispatchQueue.main.async {
            if let location = locations.last {
                self.updateLocation(location)
                self.currentLocation = location
            }
        }
    }
}
