//
//  PhotoManager.swift
//  ImageComponentDemo
//
//  Created by Narinder on 30/12/16.
//  Copyright © 2016 Narinder. All rights reserved.
//

import UIKit
import AVFoundation

typealias PhotoTakingHelperCallback = ((UIImage?) -> Void)
typealias CallbackwithURL = ((UIImage?) -> Void)

enum ImagePickerOptions: Int {
    case Camera
    case Gallery
}

private struct constant {
    static let actionTitle = "Choose From"
    static let actionMessage = "Please select an option to choose image"
    static let cameraBtnTitle = "Take Picture"
    static let galeryBtnTitle = "Select from Gallery"
    static let cancelBtnTitle = "Cancel"
}

@objc class PhotoManager:NSObject {
    
    let imagePicker = UIImagePickerController()
    internal var navController: UINavigationController!
    internal var callback: PhotoTakingHelperCallback!
    var allowEditing: Bool
    
    /*
     Intialize the navController from give reference of navigationcontroller while creating Photomanager class object.
     Callback: Callback will be call after the picking image.
     */
    init(navigationController:UINavigationController, allowEditing:Bool , callback:@escaping PhotoTakingHelperCallback) {
        
        self.navController = navigationController
        self.callback = callback
        self.allowEditing = allowEditing
        super.init()
        
        presentActionSheet()
        
        
        if checkPhotoLibraryPermission() == true {
            presentActionSheet()
        }else{
            //  CommonFunctions.sharedInstance.showAlert(titleStr: "Alert!", messageStr: "Camera permission is required, Please allow camera in settings.", actiontitleStr: "OK", withAction: false, viewController: navigationController.topViewController!)
        }
    }
    
    
    //MARK: ImagePicker Custom Functions
    /// Presenting sheet with option to select image source
    private func presentActionSheet() {
        
        var alertController = UIAlertController()
        if AppDel.isIPad
        {
            alertController = UIAlertController(title: constant.actionTitle, message: constant.actionMessage, preferredStyle: .alert)
        }else{
            alertController = UIAlertController(title: constant.actionTitle, message: constant.actionMessage, preferredStyle: .actionSheet)
        }
        
        if(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            let cameraButton = UIAlertAction(title: constant.cameraBtnTitle, style: .default, handler: { (action) -> Void in
                self.presentUIimagePicker(type: .camera)
            })
            alertController.addAction(cameraButton)
        }
        
        let  galleryButton = UIAlertAction(title: constant.galeryBtnTitle, style: .default, handler: { (action) -> Void in
            self.presentUIimagePicker(type: .photoLibrary)
        })
        alertController.addAction(galleryButton)
        
        
        let cancelButton = UIAlertAction(title: constant.cancelBtnTitle, style: .cancel, handler: { (action) -> Void in
            
        })
        alertController.addAction(cancelButton)
        
        navController.present(alertController, animated: true, completion: nil)
    }
    
    
    private func checkPhotoLibraryPermission()-> Bool {
        
        var isEnabled = false
        
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        
        switch authorizationStatus {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                if granted {
                    isEnabled = true
                    print("access granted")
                }
                else {
                    isEnabled = false
                    print("access denied")
                }
            }
        case .authorized:
            isEnabled = true
            
            print("Access authorized")
        case .denied, .restricted:
            isEnabled = false
            print("restricted")
        }
        return isEnabled
    }
    
    /*
     presentUIimagePicker will present the UIImagePicker with give type
     type: Camera or Gallery
     controller: UINavigationcontroller, navigationcontroller on with uiimagepicker will present.
     */
    private func presentUIimagePicker(type: UIImagePickerController.SourceType){
        
        imagePicker.allowsEditing = self.allowEditing
        imagePicker.sourceType = type
        imagePicker.delegate = self
        
        navController.present(imagePicker, animated: true, completion: nil)
    }
    
}
//MARK: ------------------------Class End------------------------------------



//MARK: ------------------------Class Extension------------------------------------

/*Extension for UIImagePickerControllerDelegate & UINavigationControllerDelegate
 
 
 */
extension PhotoManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if allowEditing {
            if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                callback(pickedImage)
            }
        }
        else{
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                callback(pickedImage)
            }
            
        }
        navController.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        navController.dismiss(animated: true, completion: nil)
    }
}
