//
//  UITextViewPlaceholder.swift
//  PetConnet
//
//  Created by 42Works-Worksys2 on 02/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit
import AVFoundation

@IBDesignable class PlaceholderUITextView: UIView, UITextViewDelegate {
    
    var view: UIView?
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var labelPlaceholder: UILabel!
    
    @IBInspectable var placeholderText1: String = "Enter data here ..." {
        didSet {
            labelPlaceholder.text = placeholderText1
        }
    }
    
    func commonXibSetup() {
        guard let view = loadViewFromNib() else
        {
            return
        }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        textView.delegate = self
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PlaceholderUITextView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonXibSetup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        commonXibSetup()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if !textView.hasText {
            labelPlaceholder?.isHidden = false
        }
        else {
            labelPlaceholder?.isHidden = true
        }
    }
}
