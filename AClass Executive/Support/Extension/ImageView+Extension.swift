//
//  ImageView+Extension.swift
//  PetConnet
//
//  Created by 42Works-Worksys2 on 02/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func setImage(fromUrl url:String, defaultImage image:UIImage?, isCircled:Bool = false, borderColor:UIColor? = .clear, borderWidth:CGFloat? = 1.0, isCache:Bool = true, showIndicator:Bool = true) -> Void {
        var processer:ImageProcessor?
        if isCircled {
            layer.cornerRadius = frame.size.width / 2
            layer.borderColor = borderColor?.cgColor
            layer.borderWidth = borderWidth ?? 0.5
            clipsToBounds = true
            processer = RoundCornerImageProcessor(cornerRadius: self.bounds.size.width)
        }else {
            processer = DefaultImageProcessor()
        }
        guard let url = URL(string:url) else { self.image = image; return }
        if showIndicator {
            self.kf.indicatorType = IndicatorType.activity
        }
        if isCache {
            self.kf.setImage(with: url, placeholder: image, options: [.processor(processer!)], progressBlock: nil, completionHandler: nil)
        }else {
            self.kf.setImage(with: url, placeholder: image, options: [.processor(processer!), .forceRefresh], progressBlock: nil, completionHandler: nil)
        }
    }
}

