//
//  String+Extension.swift
//  PetConnet
//
//  Created by 42Works-Worksys2 on 02/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit
import Foundation

extension String {
    var length: Int {
        return self.count
    }
    
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        let numberRegEx  = "^(?=.*\\d)(?=.*[a-zA-Z]).{8,20}$"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: self)
        return numberresult
    }
    
    func isValidPhone() -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self)
        return result
    }
    
    func attributedString(fullText: String , size : CGFloat) -> NSMutableAttributedString {
        let lightString = fullText
        let lightAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Light", size: size)!, NSAttributedString.Key.foregroundColor: AppColor.darkTextColor]
        let newAttStr = NSAttributedString(string: lightString, attributes: lightAttribute)
        
        let titleStr = self
        let boldAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: size)!, NSAttributedString.Key.foregroundColor: AppColor.darkTextColor]
        let titleAttrString = NSAttributedString(string: titleStr, attributes: boldAttribute)
        let finalStr = NSMutableAttributedString(attributedString: titleAttrString)
        finalStr.append(newAttStr)
        return finalStr
    }
    
    func attributedStringColored(fullText: String , size : CGFloat) -> NSMutableAttributedString {
        let lightString = fullText
        let lightAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Light", size: size)!, NSAttributedString.Key.foregroundColor: AppColor.primaryColor]
        let newAttStr = NSAttributedString(string: lightString, attributes: lightAttribute)
        
        let titleStr = self
        let boldAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: size)!, NSAttributedString.Key.foregroundColor: AppColor.darkTextColor]
        let titleAttrString = NSAttributedString(string: titleStr, attributes: boldAttribute)
        let finalStr = NSMutableAttributedString(attributedString: titleAttrString)
        finalStr.append(newAttStr)
        return finalStr
    }
    
    func UTCToLocal() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self) ?? Date()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt)
    }
    
    func convertDateFormat(_ from: String, _ to: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = from
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        let date = dateFormatter.date(from: self) ?? Date()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = to
        return  dateFormatter.string(from: date)
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    subscript (bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }
    
    subscript (bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = self.range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: locale) {
            ranges.append(range)
        }
        return ranges
    }
    
    func localized(forLanguage language: String = Locale.preferredLanguages.first!.components(separatedBy: "-").first!) -> String {
        
        guard let path = Bundle.main.path(forResource: language == "en" ? "Base" : language, ofType: "lproj") else {
            
            let basePath = Bundle.main.path(forResource: "Base", ofType: "lproj")!
            
            return Bundle(path: basePath)!.localizedString(forKey: self, value: "", table: nil)
        }
        
        return Bundle(path: path)!.localizedString(forKey: self, value: "", table: nil)
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    
    func getTimeWithDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd 'at' hh:mm a"
        let date = dateFormatter.date(from: self)
        return date ?? Date()
    }
}


extension Date {
    func getDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: self)
        return date
    }
    
    func getTime() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.string(from: self)
        return date
    }
    
    func timeAgoSinceDate(numericDates:Bool) -> String {
        var toDate : Date?
        var fromDate : Date?
        var endString = ""
        if Date() < self
        {
            fromDate = Date()
            toDate = self
            endString = "left"
        }
        else{
            fromDate = self
            toDate = Date()
            endString = "ago"
        }
        
        let components = Calendar.current.dateComponents([.month, .day,.hour,.minute,.second], from: fromDate ?? Date(), to: toDate ?? Date())
        
        if (components.day! >= 2) {
            return "\(components.day!) days \(endString)"
        } else if (components.day! >= 1) {
            return "1 day left"
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours \(components.minute ?? 0) mins \(endString)"
        } else if (components.hour! >= 1) {
            return "1 hour \(components.minute ?? 0) mins \(endString)"
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins \(endString)"
        } else if (components.minute! >= 1) {
            return "1 min \(endString)"
        } else if (components.second! >= 3) {
            return "\(components.second!) \(endString)"
        } else {
            return "\(components.second!) \(endString)"
        }
    }
}

