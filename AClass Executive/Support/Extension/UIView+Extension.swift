//
//  UIView+Extension.swift
//  PetConnet
//
//  Created by 42Works-Worksys2 on 02/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit
import ObjectiveC

//
// Inspectable - Design and layout for View
// cornerRadius, borderWidth, borderColor
//

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
        }
    }
    
}

//
// View for UILabel Accessory
//

extension UIView {
    
    func rightValidAccessoryView() -> UIView {
        let imgView = UIImageView(image: UIImage(named: "check_valid"))
        imgView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        imgView.backgroundColor = UIColor.clear
        return imgView
    }
    
    func rightInValidAccessoryView() -> UIView {
        let imgView = UIImageView(image: UIImage(named: "check_invalid"))
        imgView.frame = CGRect(x: self.cornerRadius, y: self.cornerRadius, width: 20, height: 20)
        imgView.backgroundColor = UIColor.clear
        return imgView
    }
}

//class GradientView: UIView {
//
//    override open class var layerClass: AnyClass {
//        get {
//            return CAGradientLayer.classForCoder()
//        }
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        let gradientLayer = self.layer as! CAGradientLayer
//        let color1 = UIColor(displayP3Red: 253/255.0, green: 55/255.0, blue: 70/255.0, alpha: 1.0).cgColor
//        let color2 =  UIColor(displayP3Red: 252/255.0, green: 204/255.0, blue: 70/255.0, alpha: 1.0).cgColor
//      // gradientLayer.locations = [0.6, 1.0]
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.3)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.7)
//        gradientLayer.colors = [color2, color1]
//    }
//}
//

extension UIView {

    private struct AssociatedKeys {
        
        static var descriptiveName = "AssociatedKeys.DescriptiveName.blurView"
        
    }

    
    private (set) var blurView: BlurView {
        
        get {
            
            if let blurView = objc_getAssociatedObject(
                
                self,
                
                &AssociatedKeys.descriptiveName
                
                ) as? BlurView {
                
                return blurView
                
            }
            
            self.blurView = BlurView(to: self)
            
            return self.blurView
            
        }
        
        set(blurView) {
            
            objc_setAssociatedObject(
                
                self,
                
                &AssociatedKeys.descriptiveName,
                
                blurView,
                
                .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                
            )
            
        }
        
        
    }
    
    
    
    class BlurView {
        
        
        
        private var superview: UIView
        
        private var blur: UIVisualEffectView?
        
        private var editing: Bool = false
        
        private (set) var blurContentView: UIView?
        
        private (set) var vibrancyContentView: UIView?
        
        
        
        var animationDuration: TimeInterval = 0.1
        
        
        
        /**
         
         * Blur style. After it is changed all subviews on
         
         * blurContentView & vibrancyContentView will be deleted.
         
         */
        
        var style: UIBlurEffect.Style = .light {
            
            didSet {
                
                guard oldValue != style,
                    
                    !editing else { return }
                
                applyBlurEffect()
                
            }
            
        }
        
        /**
         
         * Alpha component of view. It can be changed freely.
         
         */
        
        var alpha: CGFloat = 0 {
            
            didSet {
                
                guard !editing else { return }
                
                if blur == nil {
                    
                    applyBlurEffect()
                    
                }
                
                let alpha = self.alpha
                
                UIView.animate(withDuration: animationDuration) {
                    
                    self.blur?.alpha = alpha
                    
                }
                
            }
            
        }
        
        
        
        init(to view: UIView) {
            
            self.superview = view
            
        }
        
        
        
        func setup(style: UIBlurEffect.Style, alpha: CGFloat) -> Self {
            
            self.editing = true
            
            
            
            self.style = style
            
            self.alpha = alpha
            
            
            
            self.editing = false
            
            
            
            return self
            
        }
        
        
        
        func enable(isHidden: Bool = false) {
            
            if blur == nil {
                
                applyBlurEffect()
                
            }
            
            
            
            self.blur?.isHidden = isHidden
            
        }
        
        
        
        private func applyBlurEffect() {
            
            blur?.removeFromSuperview()
            
            
            
            applyBlurEffect(
                
                style: style,
                
                blurAlpha: alpha
                
            )
            
        }
        
        
        
        private func applyBlurEffect(style: UIBlurEffect.Style,
                                     
                                     blurAlpha: CGFloat) {
            
            superview.backgroundColor = UIColor.clear
            
            
            
            let blurEffect = UIBlurEffect(style: style)
            
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            
            
            
            let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
            
            let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
            
            blurEffectView.contentView.addSubview(vibrancyView)
            
            
            
            blurEffectView.alpha = blurAlpha
            
            
            
            superview.insertSubview(blurEffectView, at: 0)
            
            
            
            blurEffectView.addAlignedConstrains()
            
            vibrancyView.addAlignedConstrains()
            
            
            
            self.blur = blurEffectView
            
            self.blurContentView = blurEffectView.contentView
            
            self.vibrancyContentView = vibrancyView.contentView
            
        }
        
    }
    
    
    
    private func addAlignedConstrains() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.top)
        
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.leading)
        
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.trailing)
        
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.bottom)
        
    }
    
    
    
    private func addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute) {
        
        superview?.addConstraint(
            
            NSLayoutConstraint(
                
                item: self,
                
                attribute: attribute,
                
                relatedBy: NSLayoutConstraint.Relation.equal,
                
                toItem: superview,
                
                attribute: attribute,
                
                multiplier: 1,
                
                constant: 0
                
            )
            
        )
        
    }
    
}

extension UIView {
    
    func addConstraintsWithFormatString(formate: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: formate, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
        
    }
}


extension UIColor {
    
    static func rgbColor(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        
        return UIColor.init(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
    }
    
    static func colorFromHex(_ hex: String) -> UIColor {
        
        var hexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if hexString.hasPrefix("#") {
            
            hexString.remove(at: hexString.startIndex)
        }
        
        if hexString.count != 6 {
            
            return UIColor.magenta
        }
        
        var rgb: UInt32 = 0
        Scanner.init(string: hexString).scanHexInt32(&rgb)
        
        return UIColor.init(red: CGFloat((rgb & 0xFF0000) >> 16)/255,
                            green: CGFloat((rgb & 0x00FF00) >> 8)/255,
                            blue: CGFloat(rgb & 0x0000FF)/255,
                            alpha: 1.0)
    }
    
}

fileprivate extension UIView {
    
    fileprivate var csSafeAreaInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return self.safeAreaInsets
        } else {
            return .zero
        }
    }
    
}


